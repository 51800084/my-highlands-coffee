import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:my_highlands_coffee/ui/menu/favorite_list.dart';
import 'package:vertical_scrollable_tabview/vertical_scrollable_tabview.dart';
import '../../models/menu.dart';
import 'category_section.dart';
import 'menu_payment.dart';

class MenuScreen extends StatefulWidget {
  static const route = '/menu';
  const MenuScreen({Key? key}) : super(key: key);

  @override
  State<MenuScreen> createState() => _MenuScreenState();
}

class _MenuScreenState extends State<MenuScreen> with TickerProviderStateMixin{

  List _title = [];
  List _category1 = [];
  List _category2 = [];
  List _category3 = [];
  List _category4 = [];
  List _category5 = [];
  List _category6 = [];

  // Fetch content from the json file
  Future<void> readJson() async {
    final String response = await rootBundle.loadString('assets/menu_data.json');
    final data = await json.decode(response);
    setState(() {
      _title = data["title"];
      _category1 = data["category1"];
      _category2 = data["category2"];
      _category3 = data["category3"];
      _category4 = data["category4"];
      _category5 = data["category5"];
      _category6 = data["category6"];
    });
  }
  late TabController tabController;

  @override
  void initState() {
    readJson();
    super.initState();
    tabController = TabController(length: 6, vsync: this);
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    Category category1 = Category(
        title: _title.isNotEmpty?_title[0]["name"]: '',
        // title: _title[0]["name"],
        foods: List.generate(
            _category1.length, (index) {
              return Food(name: _category1[index]["name"] , price: _category1[index]["price"], imageUrl: _category1[index]["imageUrl"]);
        }),
    );
    Category category2 = Category(
      title: _title.isNotEmpty?_title[1]["name"]: '',
      foods: List.generate(
          _category2.length, (index) {
        return Food(name: _category2[index]["name"] , price: _category2[index]["price"], imageUrl: _category2[index]["imageUrl"]);
      }),
    );
    Category category3 = Category(
      title: _title.isNotEmpty?_title[2]["name"]: '',
      foods: List.generate(
          _category3.length, (index) {
        return Food(name: _category3[index]["name"] , price: _category3[index]["price"], imageUrl: _category3[index]["imageUrl"]);
      }),
    );
    Category category4 = Category(
      title: _title.isNotEmpty?_title[3]["name"]: '',
      foods: List.generate(
          _category4.length, (index) {
        return Food(name: _category4[index]["name"] , price: _category4[index]["price"], imageUrl: _category4[index]["imageUrl"]);
      }),
    );
    Category category5= Category(
      title: _title.isNotEmpty?_title[4]["name"]: '',
      foods: List.generate(
          _category5.length, (index) {
        return Food(name: _category5[index]["name"] , price: _category5[index]["price"], imageUrl: _category5[index]["imageUrl"]);
      }),
    );
    Category category6 = Category(
      title: _title.isNotEmpty?_title[5]["name"]: '',
      foods: List.generate(
          _category6.length, (index) {
        return Food(name: _category6[index]["name"] , price: _category6[index]["price"], imageUrl: _category6[index]["imageUrl"]);
      }),
    );

    final List<Category> data = [
      category1,
      category2,
      category3,
      category4,
      category5,
      category6
    ];

    return Scaffold(
      backgroundColor: Colors.white,
      body: VerticalScrollableTabView(
        tabController: tabController,
        listItemData: data,
        verticalScrollPosition: VerticalScrollPosition.begin,
        eachItemChild: (object, index) => CategorySection(category: object as Category),
        slivers: [
          SliverAppBar(
            pinned: true,
            snap: false,
            floating: false,
            expandedHeight: 60.0,
            leading: const Icon(Icons.place_outlined, size: 40,),
            title: GestureDetector(
              onTap: () {
                showModalBottomSheet(
                    context: context,
                    shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(topLeft: Radius.circular(10.0), topRight: Radius.circular(10.0),),
                    ),
                    builder: (BuildContext context){
                      return Container(
                        height: 296,
                        padding: const EdgeInsets.all(6.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                IconButton(
                                    onPressed: () => Navigator.pop(context),
                                    icon: const Icon(Icons.clear_outlined)),
                                SizedBox(
                                  width: MediaQuery.of(context).size.width*0.10,
                                ),
                                const Text('Chọn Phương Thức Giao Hàng'),
                                const Spacer(),
                              ],
                            ),
                            Card(
                              margin: const EdgeInsets.only(bottom: 2),
                              elevation: 0,
                              color: Colors.amber[400],
                              child: Container(
                                padding: const EdgeInsets.only(bottom: 8.0, top: 8.0),
                                child: ListTile(
                                  title: const Padding(
                                    padding: EdgeInsets.only(bottom: 8.0),
                                    child: Text('Nhận món tại', style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold,),),
                                  ),
                                  subtitle: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: const [
                                      Text('Vui lòng chọn quán', style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold),),
                                      Text('Bạn cần chọn một quán để đặt hàng', style: TextStyle(fontSize: 10, fontWeight: FontWeight.w400),),
                                    ],
                                  ),
                                  leading: ClipRRect(
                                    borderRadius: BorderRadius.circular(80),
                                    child: Image.asset('assets/images/ly.jpg'),
                                  ),
                                  trailing: SizedBox(
                                    height: 24,
                                    child: TextButton(
                                      onPressed: () => Navigator.pop(context),
                                      style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.grey[200])),
                                      child: const Text('Sửa', style: TextStyle(color: Colors.grey, fontSize: 9),),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            const SizedBox(
                              height: 8.0,
                            ),
                            Card(
                              shape: RoundedRectangleBorder(
                                side: const BorderSide(color: Colors.black12, width: 2.0),
                                borderRadius: BorderRadius.circular(4.0),
                              ),
                              margin: const EdgeInsets.only(bottom: 2),
                              elevation: 0,
                              color: Colors.white,
                              child: Container(
                                padding: const EdgeInsets.only(bottom: 8.0, top: 8.0),
                                child: ListTile(
                                  title: const Padding(
                                    padding: EdgeInsets.only(bottom: 8.0),
                                    child: Text('Giao đến', style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold,),),
                                  ),
                                  subtitle: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: const [
                                      Text('Địa chỉ hiện tại', style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold),),
                                      Text('Địa chỉ hiện tại chi tiết', style: TextStyle(fontSize: 10, fontWeight: FontWeight.w400),),
                                    ],
                                  ),
                                  leading: ClipRRect(
                                    borderRadius: BorderRadius.circular(80),
                                    child: Image.asset('assets/images/xe_may.jpg'),
                                  ),
                                  trailing: SizedBox(
                                    height: 24,
                                    child: TextButton(
                                      onPressed: () => Navigator.pop(context),
                                      style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.grey[200])),
                                      child: const Text('Sửa', style: TextStyle(color: Colors.grey, fontSize: 9),),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 220,
                              child: TextButton(
                                onPressed: () => Navigator.pop(context),
                                style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.red[900])),
                                child: const Text('Xác Nhận', style: TextStyle(color: Colors.white),),
                              ),
                            ),
                          ],
                        ),
                      );
                    }
                );
              },
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text('Nhận món tại', style: TextStyle(fontSize: 11),),
                  Row(
                    children: const [
                      Text('Vui lòng chọn quán', style: TextStyle(fontSize: 11),),
                      Icon(Icons.arrow_drop_down_outlined, size: 18,),
                    ],
                  ),
                ],
              ),
            ),
            actions: [
              IconButton(
                onPressed: () {
                  Navigator.of(context).pushNamed(FavoriteList.route);
                },
                icon: const Icon(Icons.menu_open_outlined, size: 40,),
              )
            ],
            backgroundColor: Colors.red[900],
            bottom: PreferredSize(
              preferredSize: const Size.fromHeight(40.0),
              child: Container(
                color: Colors.white,
                child: TabBar(
                  isScrollable: true,
                  controller: tabController,
                  indicatorPadding: const EdgeInsets.symmetric(horizontal: 8.0),
                  indicatorColor: Colors.red[800],
                  indicatorSize: TabBarIndicatorSize.tab,
                  unselectedLabelColor: Colors.black38,
                  labelColor: Colors.red[800],
                  indicatorWeight: 3.0,
                  tabs: data.map((e) {
                    return Tab(text: e.title,);
                  }).toList(),
                  onTap: (index) {
                    VerticalScrollableTabBarStatus.setIndex(index);
                  },
                ),
              ),
            ),
          ),
        ],
      ),
      floatingActionButton: Container(
        child: FittedBox(
          child: Stack(
            alignment: const Alignment(1.0,-0.9),
            children: [
              FloatingActionButton(
                onPressed: () {
                  Navigator.of(context).pushNamed(MenuPayment.route);
                },
                mini: true,
                child: const Icon(Icons.local_drink_rounded),
                backgroundColor: Colors.red[900],
              ),
              Container(
                child: const Center(
                  child: Text('1', style: TextStyle(color: Colors.white),),
                ),
                constraints: const BoxConstraints(minHeight: 16, minWidth: 16),
                decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      spreadRadius: 1,
                      blurRadius: 5,
                      color: Colors.black.withAlpha(50)
                    ),
                  ],
                  borderRadius: BorderRadius.circular(12),
                  color: Colors.green,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
