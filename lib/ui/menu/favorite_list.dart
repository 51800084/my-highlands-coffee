import 'package:flutter/material.dart';

class FavoriteList extends StatefulWidget {
  static const route = '/fav_list';
  const FavoriteList({Key? key}) : super(key: key);

  @override
  State<FavoriteList> createState() => _FavoriteListState();
}

class _FavoriteListState extends State<FavoriteList> {

  int itemCount = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        iconTheme: const IconThemeData(
          color: Colors.black,
        ),
        backgroundColor: Colors.white,
        centerTitle: true,
        title: const Text('Yêu Thích', style: TextStyle(color: Colors.black, fontSize: 14, ),),
      ),
      body: SafeArea(
        child: Container(
          color: Colors.grey[300],
          child: ListView.separated(
            padding: const EdgeInsets.only(top: 12.0),
            itemCount: 3,
            itemBuilder: (BuildContext context, int index) {
              return GestureDetector(
                onTap: () {

                },
                child: Container(
                  color: Colors.white,
                  width: double.infinity,
                  padding: const EdgeInsets.symmetric(vertical: 10.0),
                  child: Column(
                    children: [
                      ListTile(
                        title: const Text('Phin Sữa Đá', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),),
                        subtitle: const Text('Size S', style: TextStyle(fontSize: 12.0),),
                        leading: ClipRRect(
                          borderRadius: BorderRadius.circular(40.0),
                          child: Image.asset('assets/images/cafe1.jpg'),
                        ),
                        trailing: Icon(Icons.favorite, color: Colors.brown[400],),
                      ),
                      Container(
                        padding: const EdgeInsets.only(left: 82.0, right: 10.0),
                        child: Row(
                          children: [
                            ElevatedButton(
                              onPressed: () {
                                if (itemCount > 1) {
                                  setState(() {
                                    itemCount--;
                                  });
                                }
                              },
                              style: ElevatedButton.styleFrom(
                                  onPrimary: Colors.brown,
                                  primary: Colors.brown.shade50,
                                  onSurface: Colors.grey.shade600,
                                  minimumSize: const Size(20, 20),
                                  elevation: 0.0),
                              child: const Icon(Icons.remove, size: 10,),
                            ),
                            const SizedBox(width: 16.0,),
                            Text(
                              itemCount.toString(),
                              style: const TextStyle(fontSize: 16, color: Colors.brown),
                            ),
                            const SizedBox(width: 16.0,),
                            ElevatedButton(
                              onPressed: () {
                                setState(() {
                                  itemCount++;
                                });
                              },
                              style: ElevatedButton.styleFrom(
                                  onPrimary: Colors.brown,
                                  primary: Colors.brown.shade50,
                                  minimumSize: const Size(20, 20),
                                  onSurface: Colors.grey.shade600,
                                  elevation: 0.0),
                              child: const Icon(Icons.add, size: 12,),
                            ),
                            const Spacer(),
                            ElevatedButton(
                              onPressed: () {
                              },
                              style: ElevatedButton.styleFrom(
                                onPrimary: Colors.red[900],
                                primary: Colors.brown.shade50,
                                minimumSize: const Size(60, 26),
                                elevation: 0,
                              ),
                              // onPressed: null,
                              child: const Text(
                                'Thêm vào giỏ',
                                style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
            separatorBuilder: (BuildContext context, int index) => const SizedBox( height: 6.0,),
          ),
        ),
      ),
    );
  }
}
