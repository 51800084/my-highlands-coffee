import 'package:flutter/material.dart';
import '../../models/menu.dart';
import 'items_detail.dart';
import "package:intl/intl.dart";

class CategorySection extends StatefulWidget {
  const CategorySection({
    Key? key,
    required this.category,
  }) : super(key: key);

  final Category category;

  @override
  State<CategorySection> createState() => _CategorySectionState();
}

class _CategorySectionState extends State<CategorySection> {

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        margin: const EdgeInsets.only(bottom: 16),
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _buildSectionTileHeader(context),
            _buildFoodTileList(context),
          ],
        ),
      ),
    );
  }

  Widget _buildSectionTileHeader(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(height: 16),
        _sectionTitle(context),
        const SizedBox(height: 8.0),
        const SizedBox(height: 16),
      ],
    );
  }

  Widget _sectionTitle(BuildContext context) {
    return Row(
      children: [
        Text(
          widget.category.title,
          style: _textTheme(context).headline6,
        )
      ],
    );
  }

  Widget _buildFoodTileList(BuildContext context) {
    return Column(
      children: [
        GridView.count(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          childAspectRatio: MediaQuery.of(context).size.width / MediaQuery.of(context).size.height ,
          padding: const EdgeInsets.symmetric(vertical: 6.0),
          crossAxisCount: 2,
          children: List.generate(
            widget.category.foods.length,
                (index) {
              final food = widget.category.foods[index];
              return Column(
                children: [
                  _buildFoodTile(context: context, food: food),
                ],
              );
            },
          ),
        ),
      ],
    );
  }

  Widget _buildFoodTile({
    required BuildContext context,
    required Food food,
  }) {
    return GestureDetector(
      onTap: () {
        Navigator.push(context, MaterialPageRoute(builder: (context) => ItemsDetail(food: food)),);
      },
      child: Container(
        width: 150,
        height: 260,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10.0),
          border: Border.all(
            color: Colors.black12,
            width: 0.5,
            style: BorderStyle.solid,
          ),
          boxShadow: const [
            BoxShadow(
              color: Colors.black,
              blurRadius: 2.0,
              spreadRadius: 0.1,
              offset: Offset(2.0,0.0,),
            ),
            BoxShadow(
              color: Colors.white,
              offset: Offset(0.0,0.0),
              blurRadius: 0.0,
              spreadRadius: 0.0,
            ),
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              height: 180,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10.0),
                child: _buildFoodImage(food.imageUrl),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: _buildFoodDetail(food: food, context: context),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildFoodImage(String url) {
    return Image(image: AssetImage(url), fit: BoxFit.cover,);
  }

  Widget _buildFoodDetail({
    required BuildContext context,
    required Food food,
  }) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(food.name, style: _textTheme(context).subtitle2),
        const SizedBox(height: 16),
        Row(
          children: [
            Text( NumberFormat.currency(locale: 'vi').format(food.price) ,style: _textTheme(context).caption,),
            const SizedBox(width: 8.0),
          ],
        ),
      ],
    );
  }

  TextTheme _textTheme(context) => Theme.of(context).textTheme;
}
