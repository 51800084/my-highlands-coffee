import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../models/menu.dart';

class ItemsDetail extends StatefulWidget {
  const ItemsDetail({Key? key, required this.food}) : super(key: key);
  final Food food;

  @override
  State<ItemsDetail> createState() => _ItemsDetailState();
}

class _ItemsDetailState extends State<ItemsDetail> {

  int itemCount = 1;
  bool isBtnDisabled = true;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          body: ListView(
            children: [
              Container(
                height: MediaQuery.of(context).size.height*0.4,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(widget.food.imageUrl),
                    fit: BoxFit.cover,
                  ),
                  borderRadius: BorderRadius.vertical(bottom: Radius.circular(12.0),),
                ),
                child: Container(
                  padding: EdgeInsets.only(top: 22.0, left: 10.0, right: 10.0),
                  alignment: Alignment.topCenter,
                  child: Row(
                    children: [
                      GestureDetector(
                          onTap: () => Navigator.pop(context),
                          child: Icon(Icons.clear_outlined)),
                      Spacer(),
                      Icon(Icons.favorite_outline_outlined),
                    ],
                  ),
                ),
              ),
              Container(
                height: MediaQuery.of(context).size.height*0.34,
                padding: EdgeInsets.all(10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Text(widget.food.name, style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
                        Spacer(),
                        Text(NumberFormat.currency(locale: 'vi').format(widget.food.price), style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
                      ],
                    ),
                    SizedBox( height: 8.0,),
                    Container(
                      width: MediaQuery.of(context).size.width*0.7,
                      child: Text('Giá bán đã bao gồm 8% VAT, áp dụng từ ngày 01/02/2022 đến 31/12/2022',
                        style: TextStyle( fontSize: 10, fontWeight: FontWeight.w400),),
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Spacer(),
                        Container(
                          height: 44,
                          width: 60,
                          decoration: BoxDecoration(
                            color: Colors.brown[400],
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                          alignment: Alignment.center,
                          child: Text('S', style: TextStyle(color: Colors.white),),
                        ),
                        SizedBox(width: 16.0,),
                        Container(
                          height: 44,
                          width: 60,
                          decoration: BoxDecoration(
                            color: Colors.brown[50],
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                          alignment: Alignment.center,
                          child: Column(
                            children: [
                              Spacer(),
                              Text('M', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),),
                              Spacer(),
                              Container(
                                height: 16,
                                width: double.infinity,
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                  color: Colors.brown[100],
                                  borderRadius: BorderRadius.vertical(bottom: Radius.circular(8.0),),
                                ),
                                child: Text('+6.000đ', style: TextStyle(fontSize: 9, fontWeight: FontWeight.bold),),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(width: 16.0,),
                        Container(
                          height: 44,
                          width: 60,
                          decoration: BoxDecoration(
                            color: Colors.brown[50],
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                          alignment: Alignment.center,
                          child: Column(
                            children: [
                              Spacer(),
                              Text('L', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),),
                              Spacer(),
                              Container(
                                height: 16,
                                width: double.infinity,
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                  color: Colors.brown[100],
                                  borderRadius: BorderRadius.vertical(bottom: Radius.circular(8.0),),
                                ),
                                child: Text('+10.000đ', style: TextStyle(fontSize: 9, fontWeight: FontWeight.bold),),
                              ),
                            ],
                          ),
                        ),
                        Spacer(),
                      ],
                    ),
                    SizedBox(height: 22,),
                    Row(
                      children: [
                        Icon(Icons.event_note_outlined, size: 20,color: Colors.brown[100],),
                        SizedBox(width: 8.0,),
                        Text('Ghi chú thêm', style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w300),),
                      ],
                    ),
                    const Divider(),
                  ],
                ),
              ),
              const Divider( thickness: 2.0,),
              Container(
                height: MediaQuery.of(context).size.height*0.2,
                color: Colors.white,
                padding: EdgeInsets.all(10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Row(
                      children: [
                        Spacer(),
                        GestureDetector(
                            onTap: () {
                              if (itemCount > 1) {
                                setState(() {
                                  itemCount--;
                                });
                              }
                            },
                            child: Icon(Icons.remove_circle_outline_outlined, color: Colors.brown[50],size: 30,)),
                        SizedBox(width: 24,),
                        Text(itemCount.toString(), style: TextStyle(fontSize: 22, color: Colors.red[800], fontWeight: FontWeight.bold),),
                        SizedBox(width: 24,),
                        GestureDetector(
                            onTap: () {
                              setState(() {
                                itemCount++;
                              });
                            },
                            child: Icon(Icons.add_circle_outline_outlined, color: Colors.red[500], size: 30,)),
                        Spacer(),
                      ],
                    ),
                    SizedBox(height: 8.0,),
                    TextButton(
                      onPressed: () => Navigator.pop(context),
                      style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.red[900])),
                      child: Text('Thêm vào giỏ: ${NumberFormat.currency(locale: 'vi').format(widget.food.price*itemCount)}', style: TextStyle(color: Colors.white),),
                    ),
                  ],
                ),
              ),
            ],
          )
      ),
    );
  }
}
