import 'package:flutter/material.dart';

class MenuPayment extends StatefulWidget {
  static const route = '/menu_payment';
  const MenuPayment({Key? key}) : super(key: key);

  @override
  State<MenuPayment> createState() => _MenuPaymentState();
}

class _MenuPaymentState extends State<MenuPayment> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        iconTheme: const IconThemeData(
          color: Colors.black,
        ),
        backgroundColor: Colors.white,
        centerTitle: true,
        title: const Text('Thanh toán', style: TextStyle(color: Colors.black, fontSize: 14, ),),
        actions: [
          TextButton(
              onPressed: null,
              child: Text('Xóa tất cả', style: TextStyle(color: Colors.amber[500]),),
          ),
        ],
        elevation: 0,
      ),
      body: Container(
        color: Colors.grey[300],
        child: Column(
          children: [
            Container(
              color: Colors.grey[300],
              height: MediaQuery.of(context).size.height*0.135,
            ),
            const Info(),
            const SizedBox(height: 6.0,),
            const OrderBody(),
            const SizedBox(height: 6.0,),
            const OrderDetail(),
            SizedBox(height: MediaQuery.of(context).size.height*0.14,),
            const Payment(),
          ],
        ),
      ),
    );
  }
}

class Info extends StatelessWidget {
  const Info({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey[300],
      width: double.infinity,
      height: MediaQuery.of(context).size.height*0.275,
      child: Column(
        children: [
          Container(
            height: MediaQuery.of(context).size.height*0.068,
            width: double.infinity,
            color: Colors.white,
            padding: const EdgeInsets.all(10.0),
            child: Row(
              children: [
                Text('Đến lấy', style: TextStyle(color: Colors.brown[300], fontSize: 20.0, fontWeight: FontWeight.bold),),
                const Spacer(),
                Container(
                  height: 26,
                  width: 80,
                  decoration: BoxDecoration(
                    color: Colors.brown[400],
                    borderRadius: BorderRadius.circular(6.0),
                  ),
                  alignment: Alignment.center,
                  child: const Text('TẠI QUÁN', style: TextStyle(color: Colors.white, fontSize: 12),),
                ),
                const SizedBox(width: 8.0,),
                Container(
                  height: 26,
                  width: 80,
                  decoration: BoxDecoration(
                    color: Colors.grey[300],
                    borderRadius: BorderRadius.circular(6.0),
                  ),
                  alignment: Alignment.center,
                  child: const Text('MANG ĐI', style: TextStyle(color: Colors.black45, fontSize: 12),),
                ),
              ],
            ),
          ),
          const SizedBox(height: 1,),
          Container(
            height: MediaQuery.of(context).size.height*0.06,
            color: Colors.white,
            padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
            child: Row(
              children: [
                Icon(Icons.place_outlined, size: 16, color: Colors.brown[300],),
                const SizedBox(width: 10.0,),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Xin chọn quán', style: TextStyle(fontSize: 11, color: Colors.red[900], fontWeight: FontWeight.bold),),
                    const SizedBox(height: 2.0,),
                    Text('Bạn cần chọn một quán để tiếp tục', style: TextStyle(fontSize: 9.0,color: Colors.red[400], fontWeight: FontWeight.w400),),
                  ],
                ),
                const Spacer(),
                Icon(Icons.expand_more_outlined, size: 12, color: Colors.brown[300],),
              ],
            ),
          ),
          const SizedBox(height: 1,),
          Container(
            height: MediaQuery.of(context).size.height*0.06,
            color: Colors.white,
            padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
            child: Row(
              children: [
                Icon(Icons.account_circle_outlined, color: Colors.brown[300], size: 16,),
                const SizedBox(width: 10.0,),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Chưa có Tên liên hệ', style: TextStyle(fontSize: 11, color: Colors.grey[400],),),
                    const SizedBox(height: 2.0,),
                    const Text('+84 93 705 29 00', style: TextStyle(fontSize: 9.0,color: Colors.grey, fontWeight: FontWeight.w800),),
                  ],
                ),
                const Spacer(),
                Text('Sửa', style: TextStyle(color: Colors.brown[300], fontSize: 11),),
              ],
            ),
          ),
          const SizedBox(height: 1,),
          Container(
            height: MediaQuery.of(context).size.height*0.04,
            color: Colors.white,
            padding: const EdgeInsets.symmetric(horizontal: 10.0),
            child: Row(
              children: [
                Icon(Icons.access_time_outlined,color: Colors.brown[300], size: 16,),
                const SizedBox(width: 10.0,),
                const Text('Thời gian lấy hàng', style: TextStyle(fontSize: 11, color: Colors.grey),),
                const Spacer(),
                Icon(Icons.expand_more_outlined, size: 12, color: Colors.brown[300],),
              ],
            ),
          ),
          const SizedBox(height: 1,),
          Container(
            height: MediaQuery.of(context).size.height*0.04,
            color: Colors.white,
            padding: const EdgeInsets.symmetric(horizontal: 10.0),
            child: Row(
              children: [
                Icon(Icons.event_note_outlined,color: Colors.brown[300], size: 16,),
                const SizedBox(width: 10.0,),
                const Text('Ghi Chú Món', style: TextStyle(fontSize: 11, color: Colors.grey),),
                const Spacer(),
                Text('Sửa', style: TextStyle(color: Colors.brown[300], fontSize: 11),),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class OrderBody extends StatelessWidget {
  const OrderBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey[300],
      width: double.infinity,
      height: MediaQuery.of(context).size.height*0.14,
      child: Column(
        children: [
          Container(
            color: Colors.white,
            padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 2.0),
            child: Row(
              children: [
                const Text('Món', style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold, color: Colors.black87),),
                const Spacer(),
                Text('Thêm', style: TextStyle(fontSize: 11, color: Colors.brown[400]),),
                const SizedBox(width: 6.0,),
                Icon(Icons.add_box_outlined, color: Colors.brown[300], size: 22,),
              ],
            ),
          ),
          const SizedBox(height: 1,),
          Container(
            color: Colors.white,
            width: double.infinity,
            height: MediaQuery.of(context).size.height*0.095,
            child: Row(
              children: [
                Image.asset('assets/images/menu_payment.jpg'),
                const SizedBox(width: 10.0,),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Spacer(),
                    const Text('Phin Sữa Đá', style: TextStyle(fontWeight: FontWeight.bold),),
                    const Spacer(),
                    const Text('Size S', style: TextStyle(fontSize: 11, color: Colors.grey),),
                    SizedBox(
                      width: MediaQuery.of(context).size.width-82,
                      child: Row(
                        children: const [
                          Text('Không có ghi chú', style: TextStyle(fontSize: 10, color: Colors.grey),),
                          Spacer(),
                          Text('29.000đ', style: TextStyle(fontWeight: FontWeight.bold),),
                        ],
                      ),
                    ),
                    const SizedBox(height: 2.0,),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class OrderDetail extends StatelessWidget {
  const OrderDetail({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      width: double.infinity,
      height: MediaQuery.of(context).size.height*0.14,
      padding: const EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text('Giá bán đã bao gồm 8% VAT, áp dụng từ ngày 01/02/2022 đến 31/12/2022',
          style: TextStyle( fontSize: 9.0, color: Colors.grey),
          ),
          const Spacer(),
          SizedBox(
            width: MediaQuery.of(context).size.width-20,
            child: Row(
              children: const [
                Text('Tạm tính', style: TextStyle(fontSize: 11.0, color: Colors.grey),),
                Spacer(),
                Text('29.000đ', style: TextStyle(fontSize: 11.0, color: Colors.grey),),
              ],
            ),
          ),
          const Spacer(),
          SizedBox(
            width: MediaQuery.of(context).size.width-20,
            child: Row(
              children: const [
                Text('Tổng cộng (1 món)', style: TextStyle(fontSize: 13.0, fontWeight: FontWeight.bold),),
                Spacer(),
                Text('29.000đ', style: TextStyle(fontSize: 13.0, fontWeight: FontWeight.bold),),
              ],
            ),
          ),
          const Spacer(),
        ],
      ),
    );
  }
}

class Payment extends StatelessWidget {
  const Payment({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      width: double.infinity,
      height: MediaQuery.of(context).size.height*0.15,
      padding: const EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          SizedBox(
            width: MediaQuery.of(context).size.width-21,
            height: MediaQuery.of(context).size.height*0.03,
            child: Row(
              children: [
                Row(
                  children: [
                    Icon(Icons.credit_card, size: 20, color: Colors.brown[300],),
                    const Text('Thẻ nội địa', style: TextStyle(fontSize: 12.0, color: Colors.black, fontWeight: FontWeight.w300),),
                  ],
                ),
                const Spacer(),
                VerticalDivider(thickness: 1.0, color: Colors.brown[300],),
                Row(
                  children: [
                    Icon(Icons.discount_outlined, size: 20, color: Colors.brown[300],),
                    const Text('Thêm Ưu đãi', style: TextStyle(fontSize: 12.0, color: Colors.black, fontWeight: FontWeight.w300),),
                  ],
                ),
                const Spacer(),
              ],
            ),
          ),
          const SizedBox(height: 6.0,),
          TextButton(
            onPressed: null,
            style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.grey)),
            child: const Text('Đặt 1 Món: 29.000đ', style: TextStyle(color: Colors.white),),
          ),
        ],
      ),
    );
  }
}
