import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:my_highlands_coffee/ui/home/news_detail.dart';

class NewsAll extends StatefulWidget {
  const NewsAll({Key? key}) : super(key: key);

  @override
  State<NewsAll> createState() => _NewsAllState();
}

class _NewsAllState extends State<NewsAll> {

  List _newsAllList = [];

  Future<void> readJson() async {
    final String response = await rootBundle.loadString('assets/home_data.json');
    final data = await json.decode(response);
    setState(() {
      _newsAllList = data["news_allList"];
    });
  }

  @override
  void initState() {
    readJson();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        iconTheme: const IconThemeData(
          color: Colors.black,
        ),
        backgroundColor: Colors.white,
        centerTitle: true,
        title: const Text('Tất Cả Tin', style: TextStyle(color: Colors.black, fontSize: 14, ),),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        padding: const EdgeInsets.all(20.0),
        child: ListView.builder(
          itemCount: _newsAllList.length,
          itemBuilder: (BuildContext context, int index) {
            return GestureDetector(
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context)=> const NewsDetail()));
              },
              child: Container(
                padding: const EdgeInsets.only(bottom: 20.0),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10.0),
                  child: Image.asset(_newsAllList.isNotEmpty?_newsAllList[index]["img"]:'assets/images/tin_tuc_1.jpg', fit: BoxFit.cover,),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
