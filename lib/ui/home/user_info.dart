import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class UserInfo extends StatefulWidget {
  const UserInfo({Key? key}) : super(key: key);

  @override
  State<UserInfo> createState() => _UserInfoState();
}

class _UserInfoState extends State<UserInfo> {

  List _userInfoList = [];

  Future<void> readJson() async {
    final String response = await rootBundle.loadString('assets/home_data.json');
    final data = await json.decode(response);
    setState(() {
      _userInfoList = data["user_infoList"];
    });
  }

  @override
  void initState() {
    readJson();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return SafeArea(child: Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        iconTheme: const IconThemeData(
          color: Colors.black,
        ),
        backgroundColor: Colors.white,
        centerTitle: true,
        title: const Text('Thông tin thành viên', style: TextStyle(color: Colors.black, fontSize: 14, ),),
      ),
      body: Stack(
        children: [
          ListView(
            children: [
              Container(
                height: 200,
                width: double.infinity,
                decoration: const BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/images/hoa_tiet_nen.jpg'),
                      fit: BoxFit.cover
                  ),
                ),
                child: Container(
                  margin: const EdgeInsets.all(16.0),
                  decoration: BoxDecoration(
                    color: Colors.red,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only( left: 20.0, right: 20.0, top: 20.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text("Chào bạn!", style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold, color: Colors.white),),
                        const SizedBox(height: 10.0,),
                        SizedBox(
                          width: MediaQuery.of(context).size.width*0.5,
                          child: const Text("Tham gia chương trình thành viên của Highlands Coffee ngay để tích điểm và nhận quà hấp dẫn",
                            style: TextStyle(fontSize: 10.0, fontWeight: FontWeight.bold, color: Colors.white),),
                        ),
                        const SizedBox(height: 20.0,),
                        Container(
                          height: 30,
                          width: 140,
                          margin: const EdgeInsets.only(top: 12),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(8.0)
                          ),
                          child: TextButton(
                              style: TextButton.styleFrom(
                                primary: Colors.red,
                                textStyle: const TextStyle(fontSize: 12),
                              ),
                              onPressed: () {
                                Navigator.of(context).pushReplacementNamed('/login');
                              },
                              child: const Text('Đăng nhập/Đăng ký')),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              Container(
                color: Colors.brown[50],
                width: MediaQuery.of(context).size.width,
                height: 100,
                padding: const EdgeInsets.all(10.0),
                child: Row(
                  children: [
                    SizedBox(
                      width: MediaQuery.of(context).size.width*0.7,
                      child: const Text('Đăng ký làm thành viên Highlands Coffee để '
                          'bắt đầu thu thập Drips, nâng cấp độ và nhận nhiều lợi ít hơn',
                        style: TextStyle(fontSize: 11.0),
                      ),
                    ),
                    const Spacer(),
                    Icon(Icons.task_outlined, size: 60.0, color: Colors.green[300],),
                  ],
                ),
              ),
              Container(
                padding: const EdgeInsets.all(10.0),
                width: MediaQuery.of(context).size.width,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text('Đặc quyền thành viên', style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),),
                    ListView.builder(
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      itemCount: 3,
                      itemBuilder: (context, index) => GestureDetector(
                        onTap: () {},
                        child: Card(
                          margin: const EdgeInsets.only(bottom: 2),
                          elevation: 0,
                          child: Container(
                            padding: const EdgeInsets.only(bottom: 8.0),
                            child: ListTile(
                              title: Padding(
                                padding: const EdgeInsets.only(bottom: 8.0, top: 8.0),
                                child: Text(_userInfoList.isNotEmpty?_userInfoList[index]["title"]:'', style: const TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold,),),
                              ),
                              subtitle: Text(_userInfoList.isNotEmpty?_userInfoList[index]["subTitle"]:'', style: const TextStyle(fontSize: 12.0, fontWeight: FontWeight.w400),),
                              leading: Icon(IconData(int.parse(_userInfoList.isNotEmpty?_userInfoList[index]["icon"]:"0"),fontFamily: 'MaterialIcons'), color: Colors.brown[200], size: 26,),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    ));
  }
}