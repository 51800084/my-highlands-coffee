import 'package:flutter/material.dart';

class NewAccountGift extends StatefulWidget {
  const NewAccountGift({Key? key}) : super(key: key);

  @override
  State<NewAccountGift> createState() => _NewAccountGiftState();
}

class _NewAccountGiftState extends State<NewAccountGift> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(child: Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        iconTheme: const IconThemeData(
          color: Colors.black,
        ),
        backgroundColor: Colors.white,
        centerTitle: true,
        title: const Text('Đăng Ký Tài Khoản Mới', style: TextStyle(color: Colors.black, fontSize: 14, ),),
      ),
      body: Stack(
        children: [
          ListView(
            children: [
              Container(
                height: 140,
                width: MediaQuery.of(context).size.width,
                color: Colors.brown[50],
                child: Padding(
                  padding: const EdgeInsets.all(18.0),
                  child: Row(
                    children: [
                      Expanded(
                        child: Container(
                          margin: const EdgeInsets.only(left: 12.0),
                          padding: const EdgeInsets.only(top: 12.0),
                          alignment: Alignment.center,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: const [
                              Text('TẶNG MÃ GIẢM GIÁ 50 % CHÀO BẠN MỚI', style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),),
                              SizedBox(height: 10.0,),
                              Text('Hết hạn vào ngày 23 tháng 12 2022', style: TextStyle(fontSize: 11.0,)),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 100,
                        child: Image.asset('assets/images/the_thanh_vien.jpg'),
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(height: 20.0,),
              Container(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text('Phần Thường'),
                    const SizedBox(height: 8.0,),
                    Container(
                      padding: const EdgeInsets.all(10.0),
                      color: Colors.brown[50],
                      child: Row(
                        children: [
                          Icon(Icons.card_giftcard_outlined, size: 40.0,color: Colors.red[900],),
                          const SizedBox(width: 20.0,),
                          Text('Mã ưu đãi giảm giá 50%', style: TextStyle(color: Colors.amber[800], fontSize: 12.0),),
                        ],
                      ),
                    ),
                    const SizedBox(height: 20.0,),
                    const Text('Làm sao để hoàn thành thử thách này?', style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold),),
                    const SizedBox(height: 10.0,),
                    Container(
                      padding: const EdgeInsets.only(left: 26.0),
                      child: Column(
                        children: const [
                          Text('Nhấn vào Đăng ký ngay và hoàn tất việc đăng ký, bạn sẽ nhận ngay phần thưởng',
                          style: TextStyle(fontSize: 11.0),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(height: 10.0,),
                    const Text('Điều Khoản & Điều Kiện', style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold),),
                    const SizedBox(height: 10.0,),
                    Container(
                      padding: const EdgeInsets.only(left: 26.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: const [
                          Text('Phần thường chỉ dành riêng cho thanh viên vừa đăng ký', style: TextStyle(fontSize: 11.0),),
                          SizedBox(height: 10.0,),
                          Text('Phần thường sẽ được gửi đến bạn trong mục Ưu Đãi', style: TextStyle(fontSize: 11.0),),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Positioned(
            bottom: 0,
            child: Container(
              width: MediaQuery.of(context).size.width,
              color: Colors.white,
              height: 90,
              padding: const EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    ElevatedButton(
                      onPressed: () {
                        Navigator.of(context).pushReplacementNamed('/login');
                      },
                      style: ElevatedButton.styleFrom(
                        onPrimary: Colors.white,
                        primary: Colors.red[900],
                        minimumSize: const Size(180, 50),
                        onSurface: Colors.grey.shade600,
                      ),
                      child: const Text(
                        'Đăng Ký/Đăng Nhập',
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                  ]),
            ),
          ),
        ],
      ),
    ));
  }
}
