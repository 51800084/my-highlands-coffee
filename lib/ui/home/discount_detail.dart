import 'package:flutter/material.dart';

class DiscountDetail extends StatefulWidget {
  const DiscountDetail({Key? key}) : super(key: key);

  @override
  State<DiscountDetail> createState() => _DiscountDetailState();
}

class _DiscountDetailState extends State<DiscountDetail> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
          body: Stack(
            children: [
              ListView(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 300,
                    decoration: const BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage('assets/images/happy_week.png'),
                          fit: BoxFit.cover
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.all(20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: const [
                        Text('Ưu đãi 50% chào mừng tuần lễ vui vẻ',
                          style: TextStyle(
                            fontSize: 26.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(height: 10.0,),
                        Text('1 tháng 10, 16:00', style: TextStyle(fontSize: 12.0, fontWeight: FontWeight.bold),),
                        SizedBox(height: 10.0,),
                        Text('Highlands Coffee tặng ngay mã ưu đãi 50% dành cho Thanh Viên Mới',
                          style: TextStyle(fontSize: 14.0,),),
                        SizedBox(height: 10.0,),
                        Text('Kiểm tra tại mục Ưu Đãi và order ngay thôi!',
                          style: TextStyle(fontSize: 14.0,),),
                      ],
                    ),
                  )
                ],
              ),
              Positioned(
                top: 0,
                child: ElevatedButton(
                  onPressed: () => Navigator.pop(context),
                  style: ElevatedButton.styleFrom(
                      onPrimary: Colors.white,
                      primary: Colors.black.withOpacity(0.3),
                      shape: const CircleBorder(),
                      minimumSize: const Size(30.0, 30.0),
                      elevation: 0.0
                  ),
                  child: const Icon(
                    Icons.clear,
                    size: 20.0,
                  ),
                ),
              ),
            ],
          ),
        )
    );
  }
}
