import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/services.dart';
import 'package:my_highlands_coffee/ui/bottom_navigator_menu.dart';
import 'package:my_highlands_coffee/ui/home/new_account_gift.dart';
import 'package:my_highlands_coffee/ui/home/news_detail.dart';
import 'package:my_highlands_coffee/ui/home/user_info.dart';
import 'discount_all.dart';
import 'discount_detail.dart';
import 'news_all.dart';

class HomeScreen extends StatefulWidget {
  static const route = '/';
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  @override
  Widget build(BuildContext context) {
    String title = 'Cùng đón một ngày thú vị nhé!';
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: false,
        automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        titleSpacing: 10,
        title: Text(title, style: const TextStyle(fontSize: 15, color: Colors.black45)),
        actions: <Widget>[ Padding(
          padding: const EdgeInsets.only(right: 32.0),
          child: IconButton(
            icon: const Icon(Icons.qr_code_scanner_outlined,color: Colors.black45),
            onPressed: () {
              Navigator.of(context).pushReplacementNamed('/login');
            },
          ),
        ),]
      ),
      body: SingleChildScrollView(
        child: Column(
          children: const[
            SignIn_Up(),
            NewAccount(),
            ContentBody(),
          ],
        ),
      ),
    );
  }
}
class SignIn_Up extends StatelessWidget {
  const SignIn_Up({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(context, MaterialPageRoute(builder: (context)=> const UserInfo()));
      },
      child: Container(
        height: 200,
        width: double.infinity,
        decoration: const BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/images/hoa_tiet_nen.jpg'),
              fit: BoxFit.cover
          ),
        ),
        child: Container(
          margin: const EdgeInsets.all(16.0),
          decoration: BoxDecoration(
            color: Colors.red,
            borderRadius: BorderRadius.circular(10),
          ),
          child: Padding(
            padding: const EdgeInsets.only( left: 20.0, right: 20.0, top: 10.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    const Text("Chào bạn!", style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold, color: Colors.white),),
                    const Spacer(),
                    IconButton(
                        onPressed: () {},
                        icon: const Icon(Icons.navigate_next_outlined),
                        color: Colors.white,
                    ),
                  ],
                ),
                const Text("Tham gia chương trình thành viên của Highlands Coffee ngay để tích điểm và nhận quà hấp dẫn",
                  style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold, color: Colors.white),),
                Container(
                  height: 30,
                  width: 140,
                  margin: const EdgeInsets.only(top: 12),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(8.0)
                  ),
                  child: TextButton(
                      style: TextButton.styleFrom(
                        primary: Colors.red,
                        textStyle: const TextStyle(fontSize: 12),
                      ),
                      onPressed: () {
                        Navigator.of(context).pushReplacementNamed('/login');
                      },
                      child: const Text('Đăng nhập/Đăng ký')),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class NewAccount extends StatelessWidget {
  const NewAccount({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(context, MaterialPageRoute(builder: (context)=> const NewAccountGift()));
      },
      child: Container(
        height: 140,
        width: double.infinity,
        color: Colors.brown[50],
        child: Padding(
          padding: const EdgeInsets.all(18.0),
          child: Row(
            children: [
              SizedBox(
                height: 100,
                child: Image.asset('assets/images/the_thanh_vien.jpg'),
              ),
              Expanded(
                child: Container(
                  margin: const EdgeInsets.only(left: 12.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          const Text('ĐĂNG KÝ TÀI KHOẢN MỚI', style: TextStyle(fontSize: 11.0, fontWeight: FontWeight.bold),),
                          const Spacer(),
                          IconButton(
                            onPressed: () {},
                            icon: const Icon(Icons.navigate_next_outlined),
                            color: Colors.black,
                          ),
                        ],
                      ),
                      const Text('TẶNG MÃ GIẢM GIÁ 50% CHÀO BẠN MỚI', style: TextStyle(fontSize: 16.0,)),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class ContentBody extends StatefulWidget {
  const ContentBody({Key? key}) : super(key: key);

  @override
  State<ContentBody> createState() => _ContentBodyState();
}

class _ContentBodyState extends State<ContentBody> {

  List _homeScreenList = [];

  Future<void> readJson() async {
    final String response = await rootBundle.loadString('assets/home_data.json');
    final data = await json.decode(response);
    setState(() {
      _homeScreenList = data["home_screenList"];
    });
  }

  @override
  void initState() {
    readJson();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Container(
      padding: const EdgeInsets.all(20.0),
      margin: const EdgeInsets.only(top: 20.0),
      height: 1050,
      width: double.infinity,
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              const Text('Ưu Đãi Đặc Biệt Hôm Nay', style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold ),),
              const Spacer(),
              TextButton(
                  onPressed: () {Navigator.push(context, MaterialPageRoute(builder: (context)=> const DiscountAll()));},
                  child: Text('Tất Cả', style: TextStyle(fontSize: 20.0, color: Colors.amber[500]),)
              )
            ],
          ),
          Container(
            child: CarouselSlider.builder(
                itemCount: 2,
                itemBuilder: (context, index, pageViewIndex) => GestureDetector(
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context)=> const DiscountDetail()));
                  },
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(8.0),
                    child: Image.asset(
                      'assets/images/happy_week.png',
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              options: CarouselOptions(
                height: 200,
                autoPlay: true,
                initialPage: 0,
                reverse: true,
                viewportFraction: 1,
                autoPlayAnimationDuration: const Duration(milliseconds: 500),
              ),
            ),
          ),
          const SizedBox( height: 20.0,),
          const Text('Sản Phẩm Nổi Bật', style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
          const SizedBox(height: 12.0,),
          SizedBox(
            height: 230,
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: _homeScreenList.length,
                itemBuilder: (context, index) => GestureDetector(
                  onTap: () => showModalBottomSheet(
                      isScrollControlled: true,
                      backgroundColor: Colors.transparent,
                      shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.vertical(top: Radius.circular(10.0)),
                      ),
                      context: context,
                      builder: (context) {
                        return DraggableScrollableSheet(
                          initialChildSize: 1.0,
                          minChildSize: 1.0,
                          maxChildSize: 1.0,
                          builder: (_, controller) => Container(
                            color: Colors.white,
                            child: ListView(
                              controller: controller,
                              children: [
                                Container(
                                  height: MediaQuery.of(context).size.height*0.4,
                                  decoration: const BoxDecoration(
                                    image: DecorationImage(
                                      image: AssetImage('assets/images/order_cafe.jpg'),
                                      fit: BoxFit.cover,
                                    ),
                                    borderRadius: BorderRadius.vertical(bottom: Radius.circular(12.0),),
                                  ),
                                  child: Container(
                                    padding: const EdgeInsets.only(top: 22.0, left: 10.0, right: 10.0),
                                    alignment: Alignment.topCenter,
                                    child: Row(
                                      children: [
                                        GestureDetector(
                                            onTap: () => Navigator.pop(context),
                                            child: const Icon(Icons.clear_outlined)),
                                        const Spacer(),
                                        const Icon(Icons.favorite_outline_outlined),
                                      ],
                                    ),
                                  ),
                                ),
                                Container(
                                  height: MediaQuery.of(context).size.height*0.4,
                                  padding: const EdgeInsets.all(10.0),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        children: const [
                                          Text('Phin Sữa Đá', style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
                                          Spacer(),
                                          Text('29.000đ', style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
                                        ],
                                      ),
                                      const SizedBox( height: 8.0,),
                                      SizedBox(
                                        width: MediaQuery.of(context).size.width*0.7,
                                        child: const Text('Giá bán đã bao gồm 8% VAT, áp dụng từ ngày 01/02/2022 đến 31/12/2022',
                                          style: TextStyle( fontSize: 10, fontWeight: FontWeight.w400),),
                                      ),
                                      Row(
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          const Spacer(),
                                          Container(
                                            height: 44,
                                            width: 60,
                                            decoration: BoxDecoration(
                                              color: Colors.brown[400],
                                              borderRadius: BorderRadius.circular(8.0),
                                            ),
                                            alignment: Alignment.center,
                                            child: const Text('S', style: TextStyle(color: Colors.white),),
                                          ),
                                          const SizedBox(width: 16.0,),
                                          Container(
                                            height: 44,
                                            width: 60,
                                            decoration: BoxDecoration(
                                              color: Colors.brown[50],
                                              borderRadius: BorderRadius.circular(8.0),
                                            ),
                                            alignment: Alignment.center,
                                            child: Column(
                                              children: [
                                                const Spacer(),
                                                const Text('M', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),),
                                                const Spacer(),
                                                Container(
                                                  height: 16,
                                                  width: double.infinity,
                                                  alignment: Alignment.center,
                                                  decoration: BoxDecoration(
                                                    color: Colors.brown[100],
                                                    borderRadius: const BorderRadius.vertical(bottom: Radius.circular(8.0),),
                                                  ),
                                                  child: const Text('+6.000đ', style: TextStyle(fontSize: 9, fontWeight: FontWeight.bold),),
                                                ),
                                              ],
                                            ),
                                          ),
                                          const SizedBox(width: 16.0,),
                                          Container(
                                            height: 44,
                                            width: 60,
                                            decoration: BoxDecoration(
                                              color: Colors.brown[50],
                                              borderRadius: BorderRadius.circular(8.0),
                                            ),
                                            alignment: Alignment.center,
                                            child: Column(
                                              children: [
                                                const Spacer(),
                                                const Text('L', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),),
                                                const Spacer(),
                                                Container(
                                                  height: 16,
                                                  width: double.infinity,
                                                  alignment: Alignment.center,
                                                  decoration: BoxDecoration(
                                                    color: Colors.brown[100],
                                                    borderRadius: const BorderRadius.vertical(bottom: Radius.circular(8.0),),
                                                  ),
                                                  child: const Text('+10.000đ', style: TextStyle(fontSize: 9, fontWeight: FontWeight.bold),),
                                                ),
                                              ],
                                            ),
                                          ),
                                          const Spacer(),
                                        ],
                                      ),
                                      const SizedBox(height: 22,),
                                      Row(
                                        children: [
                                          Icon(Icons.event_note_outlined, size: 20,color: Colors.brown[100],),
                                          const SizedBox(width: 8.0,),
                                          const Text('Ghi chú thêm', style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w300),),
                                        ],
                                      ),
                                      const Divider(),
                                    ],
                                  ),
                                ),
                                const Divider( thickness: 2.0,),
                                Container(
                                  height: MediaQuery.of(context).size.height*0.2,
                                  color: Colors.white,
                                  padding: const EdgeInsets.all(10.0),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.stretch,
                                    children: [
                                      Row(
                                        children: [
                                          const Spacer(),
                                          Icon(Icons.remove_circle_outline_outlined, color: Colors.brown[50],size: 30,),
                                          const SizedBox(width: 24,),
                                          Text('1', style: TextStyle(fontSize: 22, color: Colors.red[800], fontWeight: FontWeight.bold),),
                                          const SizedBox(width: 24,),
                                          Icon(Icons.add_circle_outline_outlined, color: Colors.red[500], size: 30,),
                                          const Spacer(),
                                        ],
                                      ),
                                      const SizedBox(height: 8.0,),
                                      TextButton(
                                        onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context)=> const BottomNavigatorMenu())),
                                        style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.red[900])),
                                        child: const Text('Thêm vào giỏ: 29.000đ', style: TextStyle(color: Colors.white),),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      }
                  ),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10.0),
                      border: Border.all(
                        color: Colors.black12,
                        width: 0.5,
                        style: BorderStyle.solid,
                      ),
                      boxShadow: const [
                        BoxShadow(
                          color: Colors.black,
                          blurRadius: 2.0,
                          spreadRadius: 0.1,
                          offset: Offset(2.0,0.0,),
                        ),
                        BoxShadow(
                          color: Colors.white,
                          offset: Offset(0.0,0.0),
                          blurRadius: 0.0,
                          spreadRadius: 0.0,
                        ),
                      ],
                    ),
                    margin: const EdgeInsets.only(right: 18.0),
                    width: 150,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: 180,
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(10.0),
                            child: Image.asset(
                              _homeScreenList.isNotEmpty?_homeScreenList[index]["img"]:'assets/images/phin_sua_da.png',
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Text(
                            _homeScreenList.isNotEmpty?_homeScreenList[index]["name"]:'Phin Sữa Đá',
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                            textAlign: TextAlign.start,
                            style: const TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 16,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
            ),
          ),
          const SizedBox(
            height: 22,
          ),
          Row(
            children: [
              SizedBox(
                width: 180,
                  child: Text('Khám phá toàn bộ Menu nào!', style: TextStyle(fontSize: 16, color: Colors.red[600], fontWeight: FontWeight.bold,),)),
              const Spacer(),
              Container(
                height: 50,
                width: 110,
                decoration: BoxDecoration(
                  color: Colors.red[900],
                  borderRadius: BorderRadius.circular(8.0),
                ),
                child: TextButton(
                    style: TextButton.styleFrom(
                      primary: Colors.white,
                      textStyle: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold,),
                    ),
                    onPressed: () {
                      Navigator.push(context, MaterialPageRoute(builder: (context)=> const BottomNavigatorMenu()));
                    },
                    child: const Text('Menu'),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          Row(
            children: [
              const Text('Tin Tức', style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold ),),
              const Spacer(),
              TextButton(
                  onPressed: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context)=> const NewsAll()));
                  },
                  child: Text('Tất Cả', style: TextStyle(fontSize: 20.0, color: Colors.amber[500]),)
              )
            ],
          ),
          Container(
            child: CarouselSlider.builder(
              itemCount: 3,
              itemBuilder: (context, index, pageViewIndex) => GestureDetector(
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context)=> const NewsDetail()));
                },
                child: Stack(
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(10.0),
                      child: Image.asset(
                        _homeScreenList.isNotEmpty?_homeScreenList[index]["news"]:'assets/images/tin_tuc_1.jpg',
                        fit: BoxFit.cover,
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomLeft,
                      child: Container(
                        margin: const EdgeInsets.only(bottom: 14.0, left: 14.0),
                        height: 34,
                        width: 90,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(8.0),
                        ),
                        child: TextButton(
                          style: TextButton.styleFrom(
                            primary: Colors.red,
                            textStyle: const TextStyle(fontSize: 14,),
                          ),
                          onPressed: () {},
                          child: const Text('Xem Thêm'),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              options: CarouselOptions(
                height: 180,
                autoPlay: false,
                initialPage: 0,
                reverse: false,
                viewportFraction: 1,
                autoPlayAnimationDuration: const Duration(milliseconds: 500),
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.only(top: 50.0),
            alignment: Alignment.center,
            child: Column(
              children: [
                Icon(Icons.coffee_outlined, color: Colors.amber[500], size: 40.0,),
                const Text('Bản tin đến đây là hết. Xin cám ơn!', style: TextStyle(color: Colors.grey, fontSize: 16, fontWeight: FontWeight.w300),)
              ],
            ),
          ),
        ],
      ),
    );
  }
}

