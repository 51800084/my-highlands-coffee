import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:my_highlands_coffee/ui/home/discount_detail.dart';

class DiscountAll extends StatefulWidget {
  const DiscountAll({Key? key}) : super(key: key);

  @override
  State<DiscountAll> createState() => _DiscountAllState();
}

class _DiscountAllState extends State<DiscountAll> {

  List _newsList = [];

  Future<void> readJson() async {
    final String response = await rootBundle.loadString('assets/home_data.json');
    final data = await json.decode(response);
    setState(() {
      _newsList = data["discount_allList"];
    });
  }

  @override
  void initState() {
    readJson();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        iconTheme: const IconThemeData(
          color: Colors.black,
        ),
        backgroundColor: Colors.white,
        centerTitle: true,
        title: const Text('Tất Ưu Đãi', style: TextStyle(color: Colors.black, fontSize: 14, ),),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        padding: const EdgeInsets.all(20.0),
        child: ListView.builder(
          itemCount: _newsList.length,
          itemBuilder: (BuildContext context, int index) {
            return GestureDetector(
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context)=> const DiscountDetail()));
              },
              child: Container(
                padding: const EdgeInsets.only(bottom: 20.0),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10.0),
                  child: Image.asset(_newsList[index]["img"], fit: BoxFit.cover,),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
