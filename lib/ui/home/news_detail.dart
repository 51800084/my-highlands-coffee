import 'package:flutter/material.dart';

class NewsDetail extends StatefulWidget {
  const NewsDetail({Key? key}) : super(key: key);

  @override
  State<NewsDetail> createState() => _NewsDetailState();
}

class _NewsDetailState extends State<NewsDetail> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
          body: Stack(
            children: [
              ListView(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 300,
                    decoration: const BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage('assets/images/tin_tuc_1.jpg'),
                          fit: BoxFit.cover
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.all(20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: const [
                        Text('Mua 1 Tặng 1',
                          style: TextStyle(
                            fontSize: 26.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(height: 10.0,),
                        Text('10 tháng 5, 23:32', style: TextStyle(fontSize: 12.0, fontWeight: FontWeight.bold),),
                        SizedBox(height: 10.0,),
                        Text('Từ ngày 03-31/05/2022, Mua 1 Esp Full City Roast 1kg hoặc Cà phê truyền thống '
                            '1kg được tặng 1 ly sứ dập nổi có logo Highlands Coffee',
                          style: TextStyle(fontSize: 14.0,),),
                      ],
                    ),
                  )
                ],
              ),
              Positioned(
                top: 0,
                child: ElevatedButton(
                  onPressed: () => Navigator.pop(context),
                  style: ElevatedButton.styleFrom(
                    onPrimary: Colors.white,
                    primary: Colors.black.withOpacity(0.3),
                    shape: const CircleBorder(),
                    minimumSize: const Size(30.0, 30.0),
                    elevation: 0.0
                  ),
                  child: const Icon(
                    Icons.clear,
                    size: 20.0,
                  ),
                ),
              ),
            ],
          ),
        )
    );
  }
}
