import 'package:flutter/material.dart';

import '../account/help/feedback_support.dart';
import '../bottom_navigator_menu.dart';

class CompleteOrderDetail extends StatelessWidget {
  const CompleteOrderDetail({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        iconTheme: const IconThemeData(
          color: Colors.black,
        ),
        backgroundColor: Colors.white,
        centerTitle: true,
        title: const Text('4 tháng 6, 19:08', style: TextStyle(color: Colors.black, fontSize: 14, ),),
      ),
      body: Container(
        color: Colors.grey[300],
        child: Column(
          children: [
            Container(
              color: Colors.grey[300],
              height: MediaQuery.of(context).size.height*0.135,
            ),
            const Info(),
            const SizedBox(height: 6.0,),
            const OrderBody(),
            const SizedBox(height: 6.0,),
            const OrderDetail(),
            const SizedBox(height: 6.0,),
            Container(
              height: MediaQuery.of(context).size.height*0.06,
              color: Colors.white,
              padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
              child: Row(
                children: const [
                  Text('Hồ sơ', style: TextStyle(fontWeight: FontWeight.bold),),
                  Spacer(),
                  Icon(Icons.account_circle_outlined, color: Colors.black, size: 20,),
                  SizedBox(width: 10.0,),
                  Text('Cá nhân', style: TextStyle(color: Colors.black, fontSize: 14),),
                ],
              ),
            ),
            const SizedBox(height: 6.0,),
            Container(
              height: MediaQuery.of(context).size.height*0.08,
              width: MediaQuery.of(context).size.width,
              color: Colors.white,
              padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: const [
                  Text('Không cần dụng cụ nhựa', style: TextStyle(fontWeight: FontWeight.bold),),
                  SizedBox(height: 4.0,),
                  Text('Cám ơn bạn vì đã giúp hạn chế rác thải nhựa dùng một lần nhé!', style: TextStyle(fontSize: 10),),
                ],
              ),
            ),
            SizedBox(height: MediaQuery.of(context).size.height*0.04,),
            const Payment(),
          ],
        ),
      ),
    );
  }
}

class Info extends StatelessWidget {
  const Info({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey[300],
      width: double.infinity,
      height: MediaQuery.of(context).size.height*0.216,
      child: Column(
        children: [
          Container(
            height: 70,
            color: Colors.white,
            child: ListTile(
              title: const Text('Vivo City'),
              subtitle: const Text('4 tháng 6, 19:08'),
              leading: ClipRRect(
                borderRadius: BorderRadius.circular(40.0),
                child: Image.asset('assets/images/cafe1.jpg'),
              ),
              trailing: const Text('87.000đ'),
            ),
          ),
          const SizedBox(height: 4.0,),
          Container(
            height: MediaQuery.of(context).size.height*0.10,
            color: Colors.white,
            padding: const EdgeInsets.all(10.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Icon(Icons.check_circle_outline ,color: Colors.blue[300], size: 16,),
                    const SizedBox(width: 10.0,),
                    const Text('1058 Nguyễn Văn Linh, Phường Tân Phong', style: TextStyle(fontSize: 11, color: Colors.grey),),
                  ],
                ),
                const SizedBox(height: 10.0,),
                Row(
                  children: [
                    Icon(Icons.place_outlined,color: Colors.red[300], size: 16,),
                    const SizedBox(width: 10.0,),
                    const Text('Địa chỉ nhà', style: TextStyle(fontSize: 11, color: Colors.grey),),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class OrderBody extends StatelessWidget {
  const OrderBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey[300],
      width: double.infinity,
      height: MediaQuery.of(context).size.height*0.14,
      child: Column(
        children: [
          Container(
            color: Colors.white,
            padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
            child: Row(
              children: [
                const Text('Tóm tắt đơn hàng', style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold, color: Colors.black87),),
                const Spacer(),
                GestureDetector(
                    onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context)=> const BottomNavigatorMenu())),
                    child: const Text('Đặt lại', style: TextStyle(fontSize: 11, color: Colors.blue),)),
                const SizedBox(width: 6.0,),
              ],
            ),
          ),
          const SizedBox(height: 1,),
          Container(
            color: Colors.white,
            width: double.infinity,
            height: MediaQuery.of(context).size.height*0.095,
            child: Row(
              children: [
                Image.asset('assets/images/menu_payment.jpg'),
                const SizedBox(width: 10.0,),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Spacer(),
                    const Text('Phin Sữa Đá', style: TextStyle(fontWeight: FontWeight.bold),),
                    const Spacer(),
                    const Text('Size S', style: TextStyle(fontSize: 11, color: Colors.grey),),
                    SizedBox(
                      width: MediaQuery.of(context).size.width-82,
                      child: Row(
                        children: const [
                          Text('Không có ghi chú', style: TextStyle(fontSize: 10, color: Colors.grey),),
                          Spacer(),
                          Text('29.000đ', style: TextStyle(fontWeight: FontWeight.bold),),
                        ],
                      ),
                    ),
                    const SizedBox(height: 2.0,),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class OrderDetail extends StatelessWidget {
  const OrderDetail({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      width: double.infinity,
      height: MediaQuery.of(context).size.height*0.14,
      padding: const EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text('Giá bán đã bao gồm 8% VAT, áp dụng từ ngày 01/02/2022 đến 31/12/2022',
            style: TextStyle( fontSize: 9.0, color: Colors.grey),
          ),
          const Spacer(),
          SizedBox(
            width: MediaQuery.of(context).size.width-20,
            child: Row(
              children: const [
                Text('Tạm tính', style: TextStyle(fontSize: 11.0, color: Colors.grey),),
                Spacer(),
                Text('29.000đ', style: TextStyle(fontSize: 11.0, color: Colors.grey),),
              ],
            ),
          ),
          const Spacer(),
          SizedBox(
            width: MediaQuery.of(context).size.width-20,
            child: Row(
              children: const [
                Text('Tổng cộng (1 món)', style: TextStyle(fontSize: 13.0, fontWeight: FontWeight.bold),),
                Spacer(),
                Text('29.000đ', style: TextStyle(fontSize: 13.0, fontWeight: FontWeight.bold),),
              ],
            ),
          ),
          const Spacer(),
        ],
      ),
    );
  }
}

class Payment extends StatelessWidget {
  const Payment({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey[300],
      width: double.infinity,
      height: MediaQuery.of(context).size.height*0.15,
      padding: const EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          const SizedBox(height: 6.0,),
          TextButton(
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context)=> const FeedbackSupport()));
            },
            style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.blue[600])),
            child: const Text('Liên hệ với Highlands Coffee', style: TextStyle(color: Colors.white),),
          ),
        ],
      ),
    );
  }
}
