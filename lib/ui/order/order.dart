import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'complete_order_detail.dart';

class OrderScreen extends StatefulWidget {
  static const route = '/order';
  const OrderScreen({Key? key}) : super(key: key);

  @override
  State<OrderScreen> createState() => _OrderScreenState();
}

class _OrderScreenState extends State<OrderScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[300],
      appBar: AppBar(
        backgroundColor: Colors.white,
        automaticallyImplyLeading: false,
        centerTitle: true,
        title: const Text('Đơn hàng', style: TextStyle(color: Colors.black, fontSize: 14, ),),
      ),
      body: Center(
        child: Column(
          children: const [
            Order(),
          ],
        ),
      ),
    );
  }
}

class Order extends StatefulWidget {
  const Order({Key? key}) : super(key: key);

  @override
  State<Order> createState() => _OrderState();
}

class _OrderState extends State<Order> {

  List _orderList = [];

  Future<void> readJson() async {
    final String response = await rootBundle.loadString('assets/order_data.json');
    final data = await json.decode(response);
    setState(() {
      _orderList = data["orderList"];
    });
  }

  @override
  void initState() {
    readJson();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return DefaultTabController(
        length: 2,
        child: Column(
         crossAxisAlignment: CrossAxisAlignment.center,
         children: [
           Container(
             color: Colors.white,
             child: TabBar(
                 indicatorWeight: 1,
                 indicatorColor: Colors.red[800],
                 indicatorSize: TabBarIndicatorSize.tab,
                 unselectedLabelColor: Colors.black38,
                 // isScrollable: true,
                 labelColor: Colors.red[800],
                 tabs: const [
                   Tab(
                     child: Text('Đang diễn ra'),
                   ),
                   Tab(
                     child: Text('Lịch sử'),
                   ),
                 ]),
           ),
           SizedBox(
             height: MediaQuery.of(context).size.height - 183,
             width: double.infinity,
             child: TabBarView(
               children: [
                 Container(
                   color: Colors.grey[300],
                   padding: const EdgeInsets.all(12.0),
                   child: Column(
                     crossAxisAlignment: CrossAxisAlignment.start,
                     children: [
                       const Text('6 tháng 6, 11:44'),
                       const SizedBox(height: 12.0,),
                       Container(
                         width: double.infinity,
                         // height: 220,
                         decoration: BoxDecoration(
                           color: Colors.white,
                           borderRadius: BorderRadius.circular(10.0),
                         ),
                         padding: const EdgeInsets.all(10.0),
                         child: Column(
                           crossAxisAlignment: CrossAxisAlignment.start,
                           children: [
                             Row(
                               children: [
                                 const Text('S2683022', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),),
                                 const Spacer(),
                                 Icon(Icons.access_time, size: 22, color: Colors.amberAccent[200],),
                                 const Text('Thời gian giao hàng', style: TextStyle(fontSize: 14.0),),
                               ],
                             ),
                             const Text('Giao hàng'),
                             const SizedBox(height: 8.0,),
                             Container(
                               width: double.infinity,
                               height: 60,
                               decoration: BoxDecoration(
                                 color: Colors.brown[100],
                                 borderRadius: BorderRadius.circular(6.0),
                               ),
                               padding: const EdgeInsets.all(10.0),
                               child: Row(
                                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                 children: [
                                    Column(
                                      children: [
                                        Icon(Icons.check_circle_outline, color: Colors.blue[900],),
                                        Text('Sẵn sàng', style: TextStyle(color: Colors.blue[900], fontSize: 12.0),),
                                      ],
                                    ),
                                    Column(
                                      children: [
                                          Icon(Icons.change_circle_outlined, color: Colors.blue[300],),
                                          Text('Đã lấy hàng', style: TextStyle(color: Colors.blue[300], fontSize: 12.0),),
                                      ],
                                    ),
                                    Column(
                                      children: const [
                                        Icon(Icons.circle),
                                        Text('Hoàn tất', style: TextStyle(fontSize: 12.0),),
                                      ],
                                    ),
                                 ],
                               ),
                             ),
                             const SizedBox(height: 8.0,),
                             const ListTile(
                               title: Text("Home", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),),
                               subtitle: Text('Địa chỉ nhà rất rất là dài', style: TextStyle(fontSize: 12.0),),
                               leading: Icon(Icons.place_outlined, size: 30.0,),
                             ),
                             const Divider(thickness: 1.0,),
                             Container(
                               child: Column(
                                 crossAxisAlignment: CrossAxisAlignment.start,
                                 children: [
                                   Row(
                                     children: const [
                                       Text("THANH TOÁN"),
                                       Spacer(),
                                       Text('TỔNG TIỀN - 1 món')
                                     ],
                                   ),
                                   const SizedBox(height: 8.0,),
                                   Row(
                                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                     children: const [
                                       Text('Highlands Trả Trước', style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),),
                                       Text('77.255đ',  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),),
                                     ],
                                   ),
                                 ],
                               ),
                             ),
                           ],
                         ),
                       ),
                     ],
                   ),
                 ),
                 Container(
                   color: Colors.white,
                   child: ListView.separated(
                     padding: const EdgeInsets.all(8),
                     itemCount: 10,
                     itemBuilder: (BuildContext context, int index) {
                       return GestureDetector(
                         onTap: () {
                           Navigator.push(context, MaterialPageRoute(builder: (context)=> const CompleteOrderDetail()));
                         },
                         child: Container(
                           height: 70,
                           color: Colors.white,
                           child: ListTile(
                             title: Text(_orderList.isNotEmpty?_orderList[index]["title"]:''),
                             subtitle: Text(_orderList.isNotEmpty?_orderList[index]["subTitle"]:''),
                             leading: ClipRRect(
                               borderRadius: BorderRadius.circular(40.0),
                               child: Image.asset(_orderList.isNotEmpty?_orderList[index]["img"]:''),
                             ),
                             trailing: Text(_orderList.isNotEmpty?_orderList[index]["training"]:''),
                           ),
                         ),
                       );
                     },
                     separatorBuilder: (BuildContext context, int index) => const Divider(),
                   ),
                 ),
               ],),
           ),
         ],
        ));
  }
}



