import 'package:flutter/material.dart';
import 'package:my_highlands_coffee/ui/account/account.dart';
import 'package:my_highlands_coffee/ui/menu/menu_screen.dart';
import 'package:my_highlands_coffee/ui/order/order.dart';
import 'package:my_highlands_coffee/ui/payment/payment.dart';
import './home/home_screen.dart';

class BottomNavigatorAccount extends StatefulWidget {
  const BottomNavigatorAccount({Key? key}) : super(key: key);

  @override
  State<BottomNavigatorAccount> createState() => _BottomNavigatorAccountState();
}

class _BottomNavigatorAccountState extends State<BottomNavigatorAccount> {

  int _selectedIndex = 4;
  static const TextStyle optionStyle =
  TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static const List<Widget> _widgetOptions = <Widget>[
    HomeScreen(),
    OrderScreen(),
    MenuScreen(),
    PaymentScreen(),
    AccountScreen(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.coffee_maker_outlined),
            label: 'Trang Chủ',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.list_alt_outlined),
            label: 'Đơn Hàng',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.fastfood_outlined),
            label: 'Đặt Hàng',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_balance_wallet_outlined),
            label: 'Trả Trước',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person_outline),
            label: 'Tài Khoản',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.red[900],
        onTap: _onItemTapped,
      ),
    );
  }
}

