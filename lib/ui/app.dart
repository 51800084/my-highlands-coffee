import 'package:flutter/material.dart';
import 'package:my_highlands_coffee/ui/account/account.dart';
import 'package:my_highlands_coffee/ui/account/information/coffee_list.dart';
import 'package:my_highlands_coffee/ui/account/information/highlands_coffee_info.dart';
import 'package:my_highlands_coffee/ui/account/orthers/app_version.dart';
import 'package:my_highlands_coffee/ui/home/login.dart';
import 'package:my_highlands_coffee/ui/home/register.dart';
import 'package:my_highlands_coffee/ui/menu/favorite_list.dart';
import 'package:my_highlands_coffee/ui/menu/menu_payment.dart';
import 'package:my_highlands_coffee/ui/menu/menu_screen.dart';
import 'package:my_highlands_coffee/ui/order/order.dart';
import 'package:my_highlands_coffee/ui/payment/payment.dart';
import 'bottom_navigator.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        '/': (context) => const BottomNavigation(),
        '/login': (context) => const LoginScreen(),
        '/register': (context) => const RegisterScreen(),
        '/order': (context) => const OrderScreen(),
        '/payment': (context) => const PaymentScreen(),
        '/account': (context) => const AccountScreen(),
        '/info': (context) => const HLInfo(),
        '/coffeList': (context) => const CoffeeList(),
        '/appversion': (context) => const AppVersion(),
        '/menu': (context) => const MenuScreen(),
        '/fav_list': (context) => const FavoriteList(),
        '/menu_payment': (context) => const MenuPayment(),
      },
      initialRoute: '/',
    );
  }
}
