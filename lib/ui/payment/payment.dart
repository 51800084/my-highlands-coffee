import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class PaymentScreen extends StatefulWidget {
  static const route = '/payment';
  const PaymentScreen({Key? key}) : super(key: key);

  @override
  State<PaymentScreen> createState() => _PaymentScreenState();
}

class _PaymentScreenState extends State<PaymentScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.grey[200],
        centerTitle: true,
        elevation: 0,
        title: const Text('Highlands Trả Trước', style: TextStyle(color: Colors.black, fontSize: 14, ),),
      ),
      body: Center(
        child: Column(
          children: const [
            Header(),
          ],
        ),
      ),
    );
  }
}

class Header extends StatefulWidget {
  const Header({Key? key}) : super(key: key);

  @override
  State<Header> createState() => _HeaderState();
}

class _HeaderState extends State<Header> {

  List _paymentList = [];

  Future<void> readJson() async {
    final String response = await rootBundle.loadString('assets/payment_data.json');
    final data = await json.decode(response);
    setState(() {
      _paymentList = data["paymentList"];
    });
  }

  @override
  void initState() {
    readJson();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: 460,
      child: Column(
        children: [
          IconButton(
              onPressed: () {},
              icon: Image.asset('assets/images/icon-touch.png'),
              iconSize: 80.0,
          ),
          const SizedBox(
            height: 30.0,
          ),
          const Text('Nhấn vào nút để kích hoạt Highlands Trả Trước.', style: TextStyle(fontSize: 12),),
          const SizedBox(
            height: 34.0,
          ),
          Align(
            alignment: Alignment.topLeft,
            child: Container(
                padding: const EdgeInsets.only(left: 12.0),
                child: const Text('Khám phá trải nghiệm thanh toán', style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold,),)),
          ),
          const SizedBox(
            height: 8.0,
          ),
          ListView.builder(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemCount: 3,
            itemBuilder: (context, index) => GestureDetector(
              onTap: () {},
              child: Card(
                margin: const EdgeInsets.only(bottom: 2),
                elevation: 0,
                child: Container(
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: ListTile(
                    title: Padding(
                      padding: const EdgeInsets.only(bottom: 8.0, top: 8.0),
                      child: Text(_paymentList.isNotEmpty?_paymentList[index]["title"]:'', style: const TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold,),),
                    ),
                    subtitle: Text(_paymentList.isNotEmpty?_paymentList[index]["subTitle"]:'', style: const TextStyle(fontSize: 12.0, fontWeight: FontWeight.w400),),
                    leading: Icon(IconData(int.parse(_paymentList.isNotEmpty?_paymentList[index]["icon"]:"0"),fontFamily: 'MaterialIcons'), color: Colors.brown[200], size: 26,),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}

