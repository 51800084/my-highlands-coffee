import 'package:flutter/material.dart';

class HLInfo extends StatefulWidget {
  static const route = '/info';
  const HLInfo({Key? key}) : super(key: key);

  @override
  State<HLInfo> createState() => _HLInfoState();
}

class _HLInfoState extends State<HLInfo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(150.0),
        child: AppBar(
          flexibleSpace: Container(
            height: 200,
            width: double.infinity,
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/cafe.jpg'),
                fit: BoxFit.cover,
              ),
            ),
            child: Container(
                padding: const EdgeInsets.all(20.0),
                alignment: Alignment.bottomLeft,
                child: const Text('Khởi nguồn đam mê', style: TextStyle(fontSize: 20.0, color: Colors.white, fontWeight: FontWeight.bold),)),
          ),
          backgroundColor: Colors.transparent,
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(12.0),
          margin: const EdgeInsets.only(top: 12.0),
          child: Column(
            children: const [
              Text('Thương hiệu bắt nguồn từ cà phê Việt Nam', style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),),
              SizedBox(height: 16.0,),
              Text('Từ tin yêu với Việt Nam và niềm đam mê cà phê, năm 1999, thương hiệu'
                  'Highlands Coffe ra đời với khát vọng nâng tầm di sản cà phê lâu đời'
                  'của Việt Nam và lan rộng tinh thần tự hào, kết nối hài hòa giữa truyền thống với hiện đại.',
              style: TextStyle(fontSize: 17),),
              SizedBox(height: 14.0,),
              Text('Bắt đầu với sản phẩm cà phê đóng gói tại Hà Nội vào năm 2000,'
                  'chúng tôi đã nhanh chóng phát triển và mở rộng thành thương hiệu quán'
                  'cà phê nổi tiếng và không ngừng mở rộng hoạt động trong và ngoài nước'
                  'từ năm 2002.',
                style: TextStyle(fontSize: 17),),
              SizedBox(height: 14.0,),
              Text('Qua một chăng đường dài, chúng tôi đã không ngừng mang đến những sản phẩm'
                  'cà phê thơm ngon, sánh đượm trong không gian thoải mái và lịch sự. Những '
                  'ly cà phê của chúng tôi không chỉ đơn thuần là thức uống quen thuộc'
                  'mà còn mang trên mình một sứ mệnh văn hóa phản ánh một phần nếp sống hiện '
                  'đại của người Việt Nam.',
                style: TextStyle(fontSize: 17),),
              SizedBox(height: 14.0,),
              Text('Đến nay, Highlands Coffee vẫn duy trì khâu phân loại cà phê bằng tay để chọn'
                  'ra từng hạt cà phê chất lượng nhất, rang mới mỗi ngày và phục vụ quý khách với'
                  'nụ cười rạng rỡ trên môi. Bí quyết thành công của chúng tôi là đây:'
                  'không gian quán tuyệt vời, sản phẩm tuyệt hảo và dịch vụ chu đáo với mức giá phù hợp.',
                style: TextStyle(fontSize: 17),),
            ],
          ),
        ),
      ),
    );
  }
}
