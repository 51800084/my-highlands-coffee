import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

class CoffeeList extends StatefulWidget {
  static const route = '/coffeList';
  const CoffeeList({Key? key}) : super(key: key);

  @override
  State<CoffeeList> createState() => _CoffeeListState();
}

class _CoffeeListState extends State<CoffeeList> {

  late LatLng userPosition;
  final List<Marker> _markers = <Marker>[];

  List _coffeeList = [];

  Future<void> readJson() async {
    final String response = await rootBundle.loadString('assets/account_data.json');
    final data = await json.decode(response);
    setState(() {
      _coffeeList = data["coffeList"];
    });
  }

  @override
  void initState() {
    readJson();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        iconTheme: const IconThemeData(
          color: Colors.black,
        ),
        title: Container(
          width: double.infinity,
          height: 40,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(5.0),
          ),
          child: Container(
            padding: const EdgeInsets.only(left: 18.0),
            child: TextField(
              decoration: InputDecoration(
                suffixIcon: IconButton(
                  icon: Icon(Icons.search, color: Colors.red[900],),
                  onPressed: () {
                    print(_coffeeList);
                  },
                ),
                hintText: 'Tìm quán...',
                border: InputBorder.none,
              ),
            ),
          ),
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: SlidingUpPanel(
        minHeight: MediaQuery.of(context).size.height*0.3,
        maxHeight: MediaQuery.of(context).size.height*0.8,
        borderRadius: const BorderRadius.only(topLeft: Radius.circular(12.0), topRight: Radius.circular(12.0) ),
        panel: Container(
          child: Column(
            children: [
              Container(
                width: double.infinity,
                height: 60,
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(topLeft: Radius.circular(12.0), topRight: Radius.circular(12.0) ),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey,
                      offset: Offset(0.0, 1.0),
                      blurRadius: 5.0,
                    ),
                  ],
                ),
                child: Container(
                    padding: const EdgeInsets.all(18.0),
                    child: const Text('10 quán gần đây', style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),)),
              ),
              const SizedBox( height: 2.0,),
              Container(
                color: Colors.grey,
                height: MediaQuery.of(context).size.height*0.8-62,
                child: SingleChildScrollView(
                  physics: const ScrollPhysics(),
                  child: Column(
                    children: <Widget>[
                      ListView.builder(
                          physics: const NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemCount: 10,
                          padding: EdgeInsets.zero,
                          itemBuilder: (context, index) {
                            return Container(
                              width: double.infinity,
                              height: 183,
                              padding: const EdgeInsets.all(14.0),
                              margin: const EdgeInsets.only(top: 2.0),
                              color: Colors.white,
                              child: Row(
                                children: [
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      const SizedBox( height: 10.0,),
                                      Text(_coffeeList.isNotEmpty?_coffeeList[index]["name"]:'', style: const TextStyle(fontSize: 18, fontWeight: FontWeight.bold),),
                                      const SizedBox( height: 10.0,),
                                      SizedBox(
                                        width: 280,
                                          child: Text(_coffeeList.isNotEmpty?_coffeeList[index]["address"]:'', style: TextStyle(color: Colors.grey[600], fontSize: 12.0),)),
                                      const SizedBox( height: 10.0,),
                                      Row(
                                        children: [
                                          Icon(Icons.phone_in_talk_outlined, color: Colors.grey[600],),
                                          const SizedBox(width: 10.0,),
                                          Text(_coffeeList.isNotEmpty?_coffeeList[index]["phone"]:'',style: TextStyle(color: Colors.grey[600], fontSize: 12.0),),
                                        ],
                                      ),
                                      const SizedBox( height: 10.0,),
                                      Row(
                                        children: [
                                          Container(
                                            width: 80,
                                            height: 18,
                                            alignment: Alignment.center,
                                            decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius: BorderRadius.circular(10.0),
                                              border: Border.all(width: 1.0, color: Colors.green),
                                            ),
                                            child: const Text('Mở cửa', style: TextStyle(color: Colors.green, fontSize: 12.0),),
                                          ),
                                          const SizedBox(width: 10.0,),
                                          Text(_coffeeList.isNotEmpty?_coffeeList[index]["time"]:'', style: TextStyle(color: Colors.grey[600], fontSize: 12.0),),
                                        ],
                                      ),
                                      const SizedBox( height: 10.0,),
                                      Text('Dịch vụ đặt hàng trực tuyến sẽ có mặt sớm', style: TextStyle(color: Colors.grey[600], fontSize: 12.0),)
                                    ],
                                  ),
                                  const Spacer(),
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Column(
                                      children: [
                                        Icon(Icons.check_circle_outline_outlined, size: 50, color: Colors.grey[600],),
                                        Text(_coffeeList.isNotEmpty?_coffeeList[index]["distant"]:'', style: TextStyle(color: Colors.grey[600], fontSize: 12.0),),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            );
                          }
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
        body: FutureBuilder(
          future: findUserLocation(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            return GoogleMap(
                initialCameraPosition: CameraPosition(target: snapshot.data, zoom: 16),
              markers: Set<Marker>.of(_markers),
              mapType: MapType.terrain,
            );
          },
        ),
      ),
    );
  }

  Future<LatLng> findUserLocation() async {
    Location location = Location();
    bool _serviceEnabled;
    PermissionStatus _permissionGranted;
    LocationData _locationData;
    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return userPosition = const LatLng(51.5285582, -0.24167);
      }
    }
    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return userPosition = const LatLng(51.5285582, -0.24167);
      }
    }
    _locationData = await location.getLocation();
    _markers.add(
      Marker(
        markerId: const MarkerId('CurrentLocation'),
        position: LatLng(_locationData.latitude!, _locationData.longitude!),
      )
    );
    return userPosition = LatLng(_locationData.latitude!, _locationData.longitude!);
  }

}
