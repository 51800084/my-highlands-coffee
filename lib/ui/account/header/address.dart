import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:my_highlands_coffee/ui/account/header/address_change.dart';

class Address extends StatefulWidget {
  const Address({Key? key}) : super(key: key);

  @override
  State<Address> createState() => _AddressState();
}

class _AddressState extends State<Address> {

  List _headerList = [];

  Future<void> readJson() async {
    final String response = await rootBundle.loadString('assets/account_data.json');
    final data = await json.decode(response);
    setState(() {
      _headerList = data["headerList"];
    });
  }

  @override
  void initState() {
    readJson();
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        iconTheme: const IconThemeData(
          color: Colors.black,
        ),
        backgroundColor: Colors.white,
        centerTitle: true,
        title: const Text('Địa chỉ đã lưu', style: TextStyle(color: Colors.black, fontSize: 14, ),),
      ),
      body: SafeArea(
        child: Container(
          color: Colors.grey[300],
          child: ListView.separated(
            padding: const EdgeInsets.only(top: 12.0),
            itemCount: 3,
            itemBuilder: (BuildContext context, int index) {
              return GestureDetector(
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context)=> const AddressChange()));
                },
                child: Container(
                  color: Colors.white,
                  width: double.infinity,
                  padding: const EdgeInsets.symmetric(vertical: 10.0),
                  child: Column(
                    children: [
                      ListTile(
                        title: Text(_headerList.isNotEmpty?_headerList[index]["title"]:'', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),),
                        subtitle: Text(_headerList.isNotEmpty?_headerList[index]["subTitle"]:'', style: TextStyle(fontSize: 12.0),),
                        leading: Icon(IconData(int.parse(_headerList.isNotEmpty?_headerList[index]["icon"]:"0"),fontFamily: 'MaterialIcons'), color: Colors.brown[400], size: 30,),
                        trailing: Icon(Icons.arrow_forward_ios_outlined, color: Colors.brown[400],size: 16,),
                      ),
                    ],
                  ),
                ),
              );
            },
            separatorBuilder: (BuildContext context, int index) => const SizedBox( height: 6.0,),
          ),
        ),
      ),
    );
  }
}

