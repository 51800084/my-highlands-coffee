import 'package:flutter/material.dart';

class AddressChange extends StatefulWidget {
  const AddressChange({Key? key}) : super(key: key);

  @override
  State<AddressChange> createState() => _AddressChangeState();
}

class _AddressChangeState extends State<AddressChange> {

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
          extendBodyBehindAppBar: true,
          appBar: AppBar(
            iconTheme: const IconThemeData(
              color: Colors.black,
            ),
            backgroundColor: Colors.white,
            centerTitle: true,
            title: const Text('Thay đổi địa chỉ', style: TextStyle(color: Colors.black, fontSize: 14, ),),
          ),
          body: Stack(
            children: [
              SafeArea(
                child: Container(
                  color: Colors.grey[300],
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        color: Colors.white,
                        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                        child: TextFormField(
                          decoration: const InputDecoration(
                            border: UnderlineInputBorder(),
                            labelText: 'Tên gọi',
                          ),
                        ),
                      ),
                      Container(
                        color: Colors.white,
                        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                        child: TextFormField(
                          decoration: const InputDecoration(
                            border: UnderlineInputBorder(),
                            labelText: 'Điạ chỉ',
                          ),
                        ),
                      ),
                      Container(
                        color: Colors.white,
                        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                        child: TextFormField(
                          decoration: const InputDecoration(
                            border: UnderlineInputBorder(),
                            labelText: 'Thêm chi tiết',
                          ),
                        ),
                      ),
                      Container(
                        color: Colors.white,
                        height: 60,
                      ),
                    ],
                  ),
                ),
              ),
              Positioned(
                bottom: 0,
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  color: Colors.white,
                  height: 90,
                  padding: const EdgeInsets.all(20),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        ElevatedButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          style: ElevatedButton.styleFrom(
                            onPrimary: Colors.white,
                            primary: Colors.red[900],
                            minimumSize: Size(180, 40),
                            onSurface: Colors.grey.shade600,
                          ),
                          // onPressed: null,
                          child: const Text(
                            'Lưu địa chỉ',
                            style: TextStyle(fontSize: 16),
                          ),
                        ),
                      ]),
                ),
              ),
            ],
          ),
        )
    );
  }
}

