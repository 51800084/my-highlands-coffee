import 'package:flutter/material.dart';
import 'package:my_highlands_coffee/ui/account/account.dart';
import 'package:my_highlands_coffee/ui/bottom_navigator_account.dart';

class FeedbackSupport extends StatefulWidget {
  const FeedbackSupport({Key? key}) : super(key: key);

  @override
  State<FeedbackSupport> createState() => _FeedbackSupportState();
}

class _FeedbackSupportState extends State<FeedbackSupport> {

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
          extendBodyBehindAppBar: true,
          appBar: AppBar(
            iconTheme: const IconThemeData(
              color: Colors.black,
            ),
            backgroundColor: Colors.white,
            centerTitle: true,
            title: const Text('Phản hồi & Hỗ trợ', style: TextStyle(color: Colors.black, fontSize: 14, ),),
          ),
          body: Stack(
            children: [
              SafeArea(
                child: Container(
                  padding: EdgeInsets.only(top: 20.0, left: 10.0),
                  color: Colors.white,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Hãy cho chúng tôi biết cảm nhận của bạn', style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),),
                      Container(
                        color: Colors.white,
                        child: TextFormField(
                          decoration: const InputDecoration(
                            border: UnderlineInputBorder(),
                            labelText: 'Tên của bạn',
                          ),
                        ),
                      ),
                      Container(
                        color: Colors.white,
                        child: TextFormField(
                          initialValue: '+84',
                          decoration: const InputDecoration(
                            border: UnderlineInputBorder(),
                            labelText: 'Số điện thoại',
                          ),
                        ),
                      ),
                      Container(
                        color: Colors.white,
                        child: TextFormField(
                          decoration: const InputDecoration(
                            border: UnderlineInputBorder(),
                            labelText: 'Email',
                          ),
                        ),
                      ),
                      SizedBox(height: 10.0,),
                      GestureDetector(
                        onTap: () {
                          showModalBottomSheet(
                              context: context,
                              shape: const RoundedRectangleBorder(
                                borderRadius: BorderRadius.only(topLeft: Radius.circular(10.0), topRight: Radius.circular(10.0),),
                              ),
                              builder: (BuildContext context){
                                return Container(
                                  padding: const EdgeInsets.all(6.0),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          IconButton(
                                              onPressed: () => Navigator.pop(context),
                                              icon: const Icon(Icons.clear_outlined)),
                                          SizedBox(
                                            width: MediaQuery.of(context).size.width*0.15,
                                          ),
                                          const Text('Vui lòng chọn ngôn ngữ'),
                                          const Spacer(),
                                        ],
                                      ),
                                      OutlinedButton(
                                        onPressed: () => Navigator.pop(context),
                                        style: OutlinedButton.styleFrom(
                                          primary: Colors.black54,
                                          minimumSize: Size(MediaQuery.of(context).size.width-40, 40),
                                        ),
                                        child: const Text('Đặt hàng', style: TextStyle(fontSize: 16.0, color: Colors.black54),),
                                      ),
                                      OutlinedButton(
                                        onPressed: () => Navigator.pop(context),
                                        style: OutlinedButton.styleFrom(
                                          primary: Colors.black54,
                                          minimumSize: Size(MediaQuery.of(context).size.width-40, 40),
                                        ),
                                        child: const Text('Dịch vụ giao hàng', style: TextStyle(fontSize: 16.0, color: Colors.black54),),
                                      ),
                                      OutlinedButton(
                                        onPressed: () => Navigator.pop(context),
                                        style: OutlinedButton.styleFrom(
                                          primary: Colors.black54,
                                          minimumSize: Size(MediaQuery.of(context).size.width-40, 40),
                                        ),
                                        child: const Text('Dịch vụ quán', style: TextStyle(fontSize: 16.0, color: Colors.black54),),
                                      ),
                                      OutlinedButton(
                                        onPressed: () => Navigator.pop(context),
                                        style: OutlinedButton.styleFrom(
                                          primary: Colors.black54,
                                          minimumSize: Size(MediaQuery.of(context).size.width-40, 40),
                                        ),
                                        child: const Text('Tài khoản & Thành viên', style: TextStyle(fontSize: 16.0, color: Colors.black54),),
                                      ),
                                      OutlinedButton(
                                        onPressed: () => Navigator.pop(context),
                                        style: OutlinedButton.styleFrom(
                                          primary: Colors.black54,
                                          minimumSize: Size(MediaQuery.of(context).size.width-40, 40),
                                        ),
                                        child: const Text('Khác', style: TextStyle(fontSize: 16.0, color: Colors.black54),),
                                      ),
                                      TextButton(
                                          onPressed: () => Navigator.pop(context),
                                          child: Text('Bỏ qua', style: TextStyle(color: Colors.brown[300], fontSize: 20.0),),
                                      ),
                                    ],
                                  ),
                                );
                              }
                          );
                        },
                        child: Container(
                          color: Colors.white,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('Phân loại', style: TextStyle(color: Colors.grey[600], fontSize: 16.0),),
                              SizedBox(height: 4.0,),
                              Text('--Loại thông tin--', style: TextStyle(color: Colors.grey, fontSize: 20.0),),
                              Divider(thickness: 1,),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        color: Colors.white,
                        child: TextFormField(
                          decoration: const InputDecoration(
                            border: UnderlineInputBorder(),
                            labelText: 'Thông tin (tối đa 1500 ký tự)',
                          ),
                        ),
                      ),
                      SizedBox(height: 12.0,),
                      Text('Đính kèm'),
                      SizedBox(height: 12.0,),
                      ElevatedButton(
                          onPressed: () {
                            showModalBottomSheet(
                                context: context,
                                shape: const RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(topLeft: Radius.circular(10.0), topRight: Radius.circular(10.0),),
                                ),
                                builder: (BuildContext context){
                                  return Container(
                                    height: 220,
                                    padding: const EdgeInsets.all(6.0),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        SizedBox(height: 20.0,),
                                        OutlinedButton(
                                          onPressed: () => Navigator.pop(context),
                                          style: OutlinedButton.styleFrom(
                                            primary: Colors.black54,
                                            minimumSize: Size(MediaQuery.of(context).size.width-40, 50),
                                          ),
                                          child: const Text('Tài khoản & Thành viên', style: TextStyle(fontSize: 16.0, color: Colors.black54),),
                                        ),
                                        OutlinedButton(
                                          onPressed: () => Navigator.pop(context),
                                          style: OutlinedButton.styleFrom(
                                            primary: Colors.black54,
                                            minimumSize: Size(MediaQuery.of(context).size.width-40, 50),
                                          ),
                                          child: const Text('Khác', style: TextStyle(fontSize: 16.0, color: Colors.black54),),
                                        ),
                                        TextButton(
                                          onPressed: () => Navigator.pop(context),
                                          child: Text('Bỏ qua', style: TextStyle(color: Colors.brown[300], fontSize: 20.0),),
                                        ),
                                      ],
                                    ),
                                  );
                                }
                            );
                          },
                          style: ElevatedButton.styleFrom(
                            onPrimary: Colors.white,
                            primary: Colors.brown[300],
                            minimumSize: Size(60, 60),
                            onSurface: Colors.grey.shade600,
                          ),
                          child: Icon(Icons.add),
                      ),
                    ],
                  ),
                ),
              ),
              Positioned(
                bottom: 0,
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  color: Colors.white,
                  height: 90,
                  padding: const EdgeInsets.all(20),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        ElevatedButton(
                          onPressed: () {
                            Navigator.push(context, MaterialPageRoute(builder: (context)=> const BottomNavigatorAccount()));
                          },
                          style: ElevatedButton.styleFrom(
                            onPrimary: Colors.white,
                            primary: Colors.red[900],
                            minimumSize: Size(180, 40),
                            onSurface: Colors.grey.shade600,
                          ),
                          child: const Text(
                            'Gửi',
                            style: TextStyle(fontSize: 16),
                          ),
                        ),
                      ]),
                ),
              ),
            ],
          ),
        )
    );
  }
}