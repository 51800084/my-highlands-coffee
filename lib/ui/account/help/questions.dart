import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:my_highlands_coffee/ui/account/help/feedback_support.dart';

class Questions extends StatefulWidget {
  const Questions({Key? key}) : super(key: key);

  @override
  State<Questions> createState() => _QuestionsState();
}

class _QuestionsState extends State<Questions> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[300],
      appBar: AppBar(
        iconTheme: const IconThemeData(
          color: Colors.black,
        ),
        backgroundColor: Colors.white,
        centerTitle: true,
        title: const Text('Câu hỏi thường gặp', style: TextStyle(color: Colors.black, fontSize: 14, ),),
      ),
      body: Stack(
        children: [
          Column(
            children: const [
              Order(),
            ],
          ),
          Positioned(
            bottom: 0,
            child: Container(
              width: MediaQuery.of(context).size.width,
              color: Colors.white,
              height: 90,
              padding: const EdgeInsets.all(20),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    ElevatedButton(
                      onPressed: () {
                        Navigator.push(context, MaterialPageRoute(builder: (context)=> const FeedbackSupport()));
                      },
                      style: ElevatedButton.styleFrom(
                        onPrimary: Colors.white,
                        primary: Colors.brown[200],
                        minimumSize: const Size(180, 50),
                        onSurface: Colors.grey.shade600,
                      ),
                      child: Row(
                        children: [
                          Icon(Icons.people_outline_outlined, size: 40.0,),
                          Spacer(),
                          const Text(
                            'Hỗ trợ',
                            style: TextStyle(fontSize: 16),
                          ),
                          Spacer(),
                        ],
                      ),
                    ),
                  ]),
            ),
          ),
        ],
      ),
    );
  }
}

class Order extends StatefulWidget {
  const Order({Key? key}) : super(key: key);

  @override
  State<Order> createState() => _OrderState();
}

class _OrderState extends State<Order> {

  List _questionList = [];

  Future<void> readJson() async {
    final String response = await rootBundle.loadString('assets/account_data.json');
    final data = await json.decode(response);
    setState(() {
      _questionList = data["questionsList"];
    });
  }

  @override
  void initState() {
    readJson();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return DefaultTabController(
        length: 4,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              color: Colors.white,
              child: TabBar(
                  indicatorWeight: 1,
                  indicatorColor: Colors.red[800],
                  indicatorSize: TabBarIndicatorSize.tab,
                  unselectedLabelColor: Colors.black38,
                  isScrollable: true,
                  labelColor: Colors.red[800],
                  tabs: const [
                    Tab(
                      child: Text('CÀI ĐẶT'),
                    ),
                    Tab(
                      child: Text('ĐẶT HÀNG'),
                    ),
                    Tab(
                      child: Text('THANH TOÁN'),
                    ),
                    Tab(
                      child: Text('THANH VIÊN'),
                    ),
                  ]),
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height - 183,
              width: double.infinity,
              child: TabBarView(
                children: [
                  Container(
                    color: Colors.grey[300],
                    padding: EdgeInsets.only(top: 10.0),
                    child: ListView.separated(
                      padding: const EdgeInsets.only(top: 12.0),
                      itemCount: 3,
                      itemBuilder: (BuildContext context, int index) {
                        return GestureDetector(
                          onTap: () {
                          },
                          child: Container(
                            color: Colors.white,
                            width: double.infinity,
                            height: 60.0,
                            padding: const EdgeInsets.symmetric(vertical: 10.0),
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    SizedBox(width: 12.0,),
                                    Text(_questionList.isNotEmpty?_questionList[index]["q"]:''),
                                    Spacer(),
                                    Icon(Icons.keyboard_arrow_down_outlined),
                                    SizedBox(width: 12.0,),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                      separatorBuilder: (BuildContext context, int index) => const SizedBox( height: 6.0,),
                    ),
                  ),
                  Container(
                    color: Colors.grey[300],
                    padding: EdgeInsets.only(top: 10.0),
                    child: ListView.separated(
                      padding: const EdgeInsets.only(top: 12.0),
                      itemCount: 9,
                      itemBuilder: (BuildContext context, int index) {
                        return GestureDetector(
                          onTap: () {
                          },
                          child: Container(
                            color: Colors.white,
                            width: double.infinity,
                            height: 60.0,
                            padding: const EdgeInsets.symmetric(vertical: 10.0),
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    SizedBox(width: 12.0,),
                                    Text(_questionList[index]["q"]),
                                    Spacer(),
                                    Icon(Icons.keyboard_arrow_down_outlined),
                                    SizedBox(width: 12.0,),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                      separatorBuilder: (BuildContext context, int index) => const SizedBox( height: 6.0,),
                    ),
                  ),
                  Container(
                    color: Colors.grey[300],
                    padding: EdgeInsets.only(top: 10.0),
                    child: ListView.separated(
                      padding: const EdgeInsets.only(top: 12.0),
                      itemCount: 5,
                      itemBuilder: (BuildContext context, int index) {
                        return GestureDetector(
                          onTap: () {
                          },
                          child: Container(
                            color: Colors.white,
                            width: double.infinity,
                            height: 60.0,
                            padding: const EdgeInsets.symmetric(vertical: 10.0),
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    SizedBox(width: 12.0,),
                                    Text(_questionList[index]["q"]),
                                    Spacer(),
                                    Icon(Icons.keyboard_arrow_down_outlined),
                                    SizedBox(width: 12.0,),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                      separatorBuilder: (BuildContext context, int index) => const SizedBox( height: 6.0,),
                    ),
                  ),
                  Container(
                    color: Colors.grey[300],
                    padding: EdgeInsets.only(top: 10.0),
                    child: ListView.separated(
                      padding: const EdgeInsets.only(top: 12.0),
                      itemCount: 4,
                      itemBuilder: (BuildContext context, int index) {
                        return GestureDetector(
                          onTap: () {
                          },
                          child: Container(
                            color: Colors.white,
                            width: double.infinity,
                            height: 60.0,
                            padding: const EdgeInsets.symmetric(vertical: 10.0),
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    SizedBox(width: 12.0,),
                                    Text(_questionList[index]["q"]),
                                    Spacer(),
                                    Icon(Icons.keyboard_arrow_down_outlined),
                                    SizedBox(width: 12.0,),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                      separatorBuilder: (BuildContext context, int index) => const SizedBox( height: 6.0,),
                    ),
                  ),
                ],),
            ),
          ],
        ));
  }
}

