import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:my_highlands_coffee/ui/account/header/address.dart';
import 'package:my_highlands_coffee/ui/account/help/questions.dart';
import 'package:my_highlands_coffee/ui/account/information/coffee_list.dart';
import 'package:my_highlands_coffee/ui/account/information/highlands_coffee_info.dart';
import 'package:my_highlands_coffee/ui/account/orthers/app_version.dart';

import 'help/feedback_support.dart';

class AccountScreen extends StatefulWidget {
  static const route = '/account';
  const AccountScreen({Key? key}) : super(key: key);

  @override
  State<AccountScreen> createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen> {

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.grey[300],
        body: ListView(
            children: [
              const Header(),
              const Information(),
              const Help(),
              const More(),
              const SizedBox(
                height: 10.0,
              ),
              Container(
                padding: const EdgeInsets.only(left: 10.0),
                alignment: Alignment.bottomLeft,
                child: GestureDetector(
                  onTap: () {
                    print('Thoát ứng dụng');
                  },
                  child: Row(
                    children: const [
                      Icon(Icons.exit_to_app_outlined),
                      Text('Thoát ứng dụng')
                    ],
                  ),
                ),
              ) // child: Icon(Icons.exit_to_app_outlined)),
            ],
          ),
        ),
    );
  }
}

class Header extends StatefulWidget {
  const Header({Key? key}) : super(key: key);

  @override
  State<Header> createState() => _HeaderState();
}

class _HeaderState extends State<Header> {

  List _headerList = [];

  Future<void> readJson() async {
    final String response = await rootBundle.loadString('assets/account_data.json');
    final data = await json.decode(response);
    setState(() {
      _headerList = data["referralCode"];
    });
  }

  @override
  void initState() {
    readJson();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Container(
      color: Colors.white,
      width: double.infinity,
      height: (MediaQuery.of(context).size.height-56)*0.32,
      child: Column(
        children: [
          const SizedBox(
            height: 40,
          ),
          ListTile(
            title: const Text('Chào Bạn!', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),),
            subtitle: Text('Cập nhật thông tin', style: TextStyle(fontSize: 11, color: Colors.brown[300]),),
            leading: Icon(Icons.account_circle_outlined, size: 60, color: Colors.brown[200],),
          ),
          Row(
            children: [
              GestureDetector(
                onTap: () => showModalBottomSheet(
                  isScrollControlled: true,
                  backgroundColor: Colors.transparent,
                  shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.vertical(top: Radius.circular(10.0)),
                  ),
                  context: context,
                  builder: (context) {
                    return DraggableScrollableSheet(
                      initialChildSize: 0.955,
                      minChildSize: 0.8,
                      maxChildSize: 0.955,
                      builder: (_, controller) => Container(
                        decoration: const BoxDecoration(
                          color:  Colors.white,
                          borderRadius: BorderRadius.vertical(top: Radius.circular(10.0),),
                        ),
                        padding: const EdgeInsets.all(10.0),
                        child:ListView(
                          controller: controller,
                          children: [
                            GestureDetector(
                              onTap: () => Navigator.pop(context),
                              child: Container(
                                alignment: Alignment.centerLeft,
                                child: const Icon(Icons.clear_outlined),
                              ),
                            ),
                            SizedBox(
                              width: MediaQuery.of(context).size.width,
                              height: 180,
                              child: Image.asset('assets/images/account_hearder.jpg'),
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 14.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  const Text('Hãy chia sẽ hành trình tuyệt vời với quyền lời và trải nghiệm kinh ngạc tới bạn bè.', textAlign: TextAlign.center,
                                    style: TextStyle(fontSize: 13.0),
                                  ),
                                  const SizedBox(height: 20,),
                                  Container(
                                    height: 50,
                                    width: MediaQuery.of(context).size.width*0.75,
                                    decoration: BoxDecoration(
                                      color: Colors.brown.shade300,
                                      borderRadius: BorderRadius.circular(10.0),
                                    ),
                                    padding: const EdgeInsets.symmetric(horizontal: 20.0),
                                    child: Row(
                                      children: const [
                                        Spacer(),
                                        Text('pmcBvo', style: TextStyle(fontSize: 20.0, color: Colors.white),),
                                        Spacer(),
                                        Icon(Icons.share_outlined, color: Colors.white, size: 32,),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            const SizedBox( height: 20.0,),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text('Cách sử dụng', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0),),
                                ListView.builder(
                                  scrollDirection: Axis.vertical,
                                  shrinkWrap: true,
                                  itemCount: 3,
                                  itemBuilder: (context, index) => GestureDetector(
                                    onTap: () {},
                                    child: Card(
                                      margin: const EdgeInsets.only(bottom: 2),
                                      elevation: 0,
                                      child: Container(
                                        padding: const EdgeInsets.only(bottom: 8.0),
                                        child: ListTile(
                                          title: Padding(
                                            padding: const EdgeInsets.only(bottom: 8.0, top: 8.0),
                                            child: Text(_headerList.isNotEmpty?_headerList[index]["title"]:'', style:TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold,),),
                                          ),
                                          subtitle: Text(_headerList.isNotEmpty?_headerList[index]["subTitle"]:'', style: TextStyle(fontSize: 12.0, fontWeight: FontWeight.w400),),
                                          leading: Icon(IconData(int.parse(_headerList.isNotEmpty?_headerList[index]["icon"]:"0"),fontFamily: 'MaterialIcons'), color: Colors.brown[200], size: 30,),
                                        ),
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                ),
                child: Container(
                  margin : const EdgeInsets.only(left: 20),
                  height: (MediaQuery.of(context).size.height-56)*0.1,
                  width: (MediaQuery.of(context).size.width-60)/3,
                  decoration: BoxDecoration(
                    color: Colors.brown[50],
                    borderRadius: BorderRadius.circular(10.0)
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(6.0),
                    child: Column(
                      children: [
                        Icon(Icons.people_outline_outlined,size: 30, color: Colors.red[800],),
                        Text('Mã giới thiệu', style: TextStyle(fontSize: 11, color: Colors.red[800]),)
                      ],
                    ),
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context)=> const Address()));
                },
                child: Container(
                  margin : const EdgeInsets.only(left: 10, right: 10),
                  height: (MediaQuery.of(context).size.height-56)*0.1,
                  width: (MediaQuery.of(context).size.width-60)/3,
                  decoration: BoxDecoration(
                      color: Colors.brown[50],
                      borderRadius: BorderRadius.circular(10.0)
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(6.0),
                    child: Column(
                      children: [
                        Icon(Icons.place_outlined,size: 30, color: Colors.red[800],),
                        Text('Địa chỉ', style: TextStyle(fontSize: 11, color: Colors.red[800]),)
                      ],
                    ),
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                  showDialog(
                      context: context,
                      builder: (_) => AlertDialog(
                          title: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                GestureDetector(
                                    onTap: () {
                                      Navigator.of(context).pop();
                                    },
                                    child: const Icon(Icons.clear_outlined, color: Colors.black45,)),
                              ]
                          ),
                          content: SingleChildScrollView(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  const Text('Vui lòng đưa mã QR để quét mã tại của hàng',textAlign: TextAlign.center, style: TextStyle( fontSize: 14.0),),
                                  const Icon(Icons.qr_code_2_outlined, size: 200, color: Colors.black45,),
                                  const SizedBox(height: 20.0,),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      const Text('Cập nhật sau 0 giây', style: TextStyle(fontSize: 12.0, color: Colors.black),),
                                      GestureDetector(
                                          onTap: () {
                                            Navigator.of(context).pop();
                                          },
                                          child: Text('Cập nhật', style: TextStyle(fontSize: 12.0, color: Colors.brown[300]),)),
                                    ],
                                  )
                                ],
                              ),
                          ),
                      )
                  );
                },
                child: Container(
                  margin : const EdgeInsets.only(right: 20),
                  height: (MediaQuery.of(context).size.height-56)*0.1,
                  width: (MediaQuery.of(context).size.width-60)/3,
                  decoration: BoxDecoration(
                      color: Colors.brown[50],
                      borderRadius: BorderRadius.circular(10.0)
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(6.0),
                    child: Column(
                      children: [
                        Icon(Icons.qr_code_2_outlined,size: 30, color: Colors.red[800],),
                        Text('Mã QR', style: TextStyle(fontSize: 11, color: Colors.red[800]),)
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class Information extends StatelessWidget {
  const Information({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 6.0),
      height: 100,
      width: double.infinity,
      color: Colors.white,
      child: Container(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          children: [
            Row(
              children: const [
                Icon(Icons.info_outline, color: Colors.grey, size: 20,),
                SizedBox( width: 6.0,),
                Text('Thông tin chung', style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold ),),
              ],
            ),
            const SizedBox( height: 6.0,),
            GestureDetector(
              onTap: () {
                Navigator.of(context).pushNamed(HLInfo.route);
              },
              child: Row(
                children: const [
                  Text('Về Highlands Coffee', style: TextStyle(color: Colors.grey),),
                  Spacer(),
                  Icon(Icons.keyboard_arrow_right_outlined, color: Colors.grey,),
                ],
              ),
            ),
            const SizedBox(height: 6.0,),
            GestureDetector(
              onTap: () {
                Navigator.of(context).pushNamed(CoffeeList.route);
              },
              child: Row(
                children: const [
                  Text('Danh sách quán', style: TextStyle(color: Colors.grey),),
                  Spacer(),
                  Icon(Icons.keyboard_arrow_right_outlined, color: Colors.grey,),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class Help extends StatelessWidget {
  const Help({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 6.0),
      height: 100,
      width: double.infinity,
      color: Colors.white,
      child: Container(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          children: [
            Row(
              children: const [
                Icon(Icons.help_outline_outlined, color: Colors.grey, size: 20,),
                SizedBox( width: 6.0,),
                Text('Trung tâm trợ giúp', style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold ),),
              ],
            ),
            const SizedBox( height: 6.0,),
            GestureDetector(
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context)=> const Questions()));
              },
              child: Row(
                children: const [
                  Text('Câu hỏi thường gặp ', style: TextStyle(color: Colors.grey),),
                  Spacer(),
                  Icon(Icons.keyboard_arrow_right_outlined, color: Colors.grey,),
                ],
              ),
            ),
            const SizedBox(height: 6.0,),
            GestureDetector(
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context)=> const FeedbackSupport()));
              },
              child: Row(
                children: const [
                  Text('Phản hồi & Hỗ trợ', style: TextStyle(color: Colors.grey),),
                  Spacer(),
                  Icon(Icons.keyboard_arrow_right_outlined, color: Colors.grey,),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class More extends StatelessWidget {
  const More({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 6.0),
      height: 130,
      width: double.infinity,
      color: Colors.white,
      child: Container(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          children: [
            Row(
              children: const [
                Icon(Icons.arrow_circle_right_outlined, color: Colors.grey, size: 20,),
                SizedBox( width: 6.0,),
                Text('Khác', style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold ),),
              ],
            ),
            const SizedBox( height: 6.0,),
            GestureDetector(
              onTap: () {
                showModalBottomSheet(
                    context: context,
                    shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(topLeft: Radius.circular(10.0), topRight: Radius.circular(10.0),),
                    ),
                    builder: (BuildContext context){
                      return Container(
                        height: 168,
                        padding: const EdgeInsets.all(6.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                IconButton(
                                    onPressed: () => Navigator.pop(context),
                                    icon: const Icon(Icons.clear_outlined)),
                                SizedBox(
                                  width: MediaQuery.of(context).size.width*0.15,
                                ),
                                const Text('Vui lòng chọn ngôn ngữ'),
                                const Spacer(),
                              ],
                            ),
                            OutlinedButton(
                                onPressed: () => Navigator.pop(context),
                                style: OutlinedButton.styleFrom(
                                  primary: Colors.red,
                                  side: const BorderSide(width: 1, color: Colors.red),
                                ),
                                child: const Text('Tiếng Việt'),
                            ),
                            OutlinedButton(
                              onPressed: () => Navigator.pop(context),
                              style: OutlinedButton.styleFrom(
                                primary: Colors.black12,
                              ),
                              child: const Text('English'),
                            ),
                          ],
                        ),
                      );
                    }
                );
              },
              child: Row(
                children: [
                  const Text('Ngôn ngữ', style: TextStyle(color: Colors.grey),),
                  const Spacer(),
                  Text('Tiếng Việt', style: TextStyle(color: Colors.brown[300], fontWeight: FontWeight.bold),),
                  const Icon(Icons.keyboard_arrow_right_outlined, color: Colors.grey,),
                ],
              ),
            ),
            const SizedBox(height: 6.0,),
            GestureDetector(
              onTap: () => showModalBottomSheet(
                  isScrollControlled: true,
                  backgroundColor: Colors.transparent,
                  shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.vertical(top: Radius.circular(10.0)),
                  ),
                  context: context,
                  builder: (context) {
                    return DraggableScrollableSheet(
                      initialChildSize: 0.90,
                      minChildSize: 0.7,
                      maxChildSize: 0.90,
                      builder: (_, controller) => Container(
                        decoration: const BoxDecoration(
                          color:  Colors.white,
                          borderRadius: BorderRadius.vertical(top: Radius.circular(10.0),),
                        ),
                        padding: const EdgeInsets.all(10.0),
                        child:ListView(
                          controller: controller,
                          children: [
                            GestureDetector(
                              onTap: () => Navigator.pop(context),
                              child: Container(
                                alignment: Alignment.centerLeft,
                                child: const Icon(Icons.clear_outlined),
                              ),
                            ),
                            const Center(child: Text('Điều Khoản & Điều Kiện', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),)),
                            const SizedBox(
                              height: 14,
                            ),
                            const Text('Từ tin yêu với Việt Nam và niềm đam mê cà phê, năm 1999, thương hiệu'
                                'Highlands Coffe ra đời với khát vọng nâng tầm di sản cà phê lâu đời'
                                'của Việt Nam và lan rộng tinh thần tự hào, kết nối hài hòa giữa truyền thống với hiện đại.'
                                'Bắt đầu với sản phẩm cà phê đóng gói tại Hà Nội vào năm 2000,'
                                'chúng tôi đã nhanh chóng phát triển và mở rộng thành thương hiệu quán'
                                'cà phê nổi tiếng và không ngừng mở rộng hoạt động trong và ngoài nước'
                                'từ năm 2002.'
                                'Qua một chăng đường dài, chúng tôi đã không ngừng mang đến những sản phẩm'
                                'cà phê thơm ngon, sánh đượm trong không gian thoải mái và lịch sự. Những '
                                'ly cà phê của chúng tôi không chỉ đơn thuần là thức uống quen thuộc'
                                'mà còn mang trên mình một sứ mệnh văn hóa phản ánh một phần nếp sống hiện '
                                'đại của người Việt Nam.'
                                'Đến nay, Highlands Coffee vẫn duy trì khâu phân loại cà phê bằng tay để chọn'
                                'ra từng hạt cà phê chất lượng nhất, rang mới mỗi ngày và phục vụ quý khách với'
                                'nụ cười rạng rỡ trên môi. Bí quyết thành công của chúng tôi là đây:'
                                'không gian quán tuyệt vời, sản phẩm tuyệt hảo và dịch vụ chu đáo với mức giá phù hợp.'),
                            const Text('Từ tin yêu với Việt Nam và niềm đam mê cà phê, năm 1999, thương hiệu'
                                'Highlands Coffe ra đời với khát vọng nâng tầm di sản cà phê lâu đời'
                                'của Việt Nam và lan rộng tinh thần tự hào, kết nối hài hòa giữa truyền thống với hiện đại.'
                                'Bắt đầu với sản phẩm cà phê đóng gói tại Hà Nội vào năm 2000,'
                                'chúng tôi đã nhanh chóng phát triển và mở rộng thành thương hiệu quán'
                                'cà phê nổi tiếng và không ngừng mở rộng hoạt động trong và ngoài nước'
                                'từ năm 2002.'
                                'Qua một chăng đường dài, chúng tôi đã không ngừng mang đến những sản phẩm'
                                'cà phê thơm ngon, sánh đượm trong không gian thoải mái và lịch sự. Những '
                                'ly cà phê của chúng tôi không chỉ đơn thuần là thức uống quen thuộc'
                                'mà còn mang trên mình một sứ mệnh văn hóa phản ánh một phần nếp sống hiện '
                                'đại của người Việt Nam.'
                                'Đến nay, Highlands Coffee vẫn duy trì khâu phân loại cà phê bằng tay để chọn'
                                'ra từng hạt cà phê chất lượng nhất, rang mới mỗi ngày và phục vụ quý khách với'
                                'nụ cười rạng rỡ trên môi. Bí quyết thành công của chúng tôi là đây:'
                                'không gian quán tuyệt vời, sản phẩm tuyệt hảo và dịch vụ chu đáo với mức giá phù hợp.'),
                            const Text('Từ tin yêu với Việt Nam và niềm đam mê cà phê, năm 1999, thương hiệu'
                                'Highlands Coffe ra đời với khát vọng nâng tầm di sản cà phê lâu đời'
                                'của Việt Nam và lan rộng tinh thần tự hào, kết nối hài hòa giữa truyền thống với hiện đại.'
                                'Bắt đầu với sản phẩm cà phê đóng gói tại Hà Nội vào năm 2000,'
                                'chúng tôi đã nhanh chóng phát triển và mở rộng thành thương hiệu quán'
                                'cà phê nổi tiếng và không ngừng mở rộng hoạt động trong và ngoài nước'
                                'từ năm 2002.'
                                'Qua một chăng đường dài, chúng tôi đã không ngừng mang đến những sản phẩm'
                                'cà phê thơm ngon, sánh đượm trong không gian thoải mái và lịch sự. Những '
                                'ly cà phê của chúng tôi không chỉ đơn thuần là thức uống quen thuộc'
                                'mà còn mang trên mình một sứ mệnh văn hóa phản ánh một phần nếp sống hiện '
                                'đại của người Việt Nam.'
                                'Đến nay, Highlands Coffee vẫn duy trì khâu phân loại cà phê bằng tay để chọn'
                                'ra từng hạt cà phê chất lượng nhất, rang mới mỗi ngày và phục vụ quý khách với'
                                'nụ cười rạng rỡ trên môi. Bí quyết thành công của chúng tôi là đây:'
                                'không gian quán tuyệt vời, sản phẩm tuyệt hảo và dịch vụ chu đáo với mức giá phù hợp.'),
                            const Text('Từ tin yêu với Việt Nam và niềm đam mê cà phê, năm 1999, thương hiệu'
                                'Highlands Coffe ra đời với khát vọng nâng tầm di sản cà phê lâu đời'
                                'của Việt Nam và lan rộng tinh thần tự hào, kết nối hài hòa giữa truyền thống với hiện đại.'
                                'Bắt đầu với sản phẩm cà phê đóng gói tại Hà Nội vào năm 2000,'
                                'chúng tôi đã nhanh chóng phát triển và mở rộng thành thương hiệu quán'
                                'cà phê nổi tiếng và không ngừng mở rộng hoạt động trong và ngoài nước'
                                'từ năm 2002.'
                                'Qua một chăng đường dài, chúng tôi đã không ngừng mang đến những sản phẩm'
                                'cà phê thơm ngon, sánh đượm trong không gian thoải mái và lịch sự. Những '
                                'ly cà phê của chúng tôi không chỉ đơn thuần là thức uống quen thuộc'
                                'mà còn mang trên mình một sứ mệnh văn hóa phản ánh một phần nếp sống hiện '
                                'đại của người Việt Nam.'
                                'Đến nay, Highlands Coffee vẫn duy trì khâu phân loại cà phê bằng tay để chọn'
                                'ra từng hạt cà phê chất lượng nhất, rang mới mỗi ngày và phục vụ quý khách với'
                                'nụ cười rạng rỡ trên môi. Bí quyết thành công của chúng tôi là đây:'
                                'không gian quán tuyệt vời, sản phẩm tuyệt hảo và dịch vụ chu đáo với mức giá phù hợp.'),
                          ],
                        ),
                      ),
                    );
                  },
              ),
              child: Row(
                children: const [
                  Text('Điều khoản & Điều kiện', style: TextStyle(color: Colors.grey),),
                  Spacer(),
                  Icon(Icons.keyboard_arrow_right_outlined, color: Colors.grey,),
                ],
              ),
            ),
            const SizedBox(height: 6.0,),
            GestureDetector(
              onTap: () {
                Navigator.of(context).pushNamed(AppVersion.router);
              },
              child: Row(
                children: const [
                  Text('Về Ứng dụng', style: TextStyle(color: Colors.grey),),
                  Spacer(),
                  Icon(Icons.keyboard_arrow_right_outlined, color: Colors.grey,),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

