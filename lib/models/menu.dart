class Category {
  String title;
  List<Food> foods;

  Category({
    required this.title,
    required this.foods,
  });
}

class Food {
  String name;
  int price;
  String imageUrl;

  Food({
    required this.name,
    required this.price,
    required this.imageUrl,
  });
}